<?php
    require_once 'configuration.php';
    session_start();
    if(isset($_REQUEST['a']) && $_REQUEST['user'] == $USERNAME && $_REQUEST['password'] == $PASSWORD)
    {
        $_SESSION['account'] = 1;
        header("Location: ./");
    }
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Login</title>

        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/bootstrap-reset.css" rel="stylesheet">
        <!--external css-->
        <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
        <!-- Custom styles for this template -->
        <link href="css/style.css" rel="stylesheet">
        <link href="css/style-responsive.css" rel="stylesheet" />

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
        <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="login-body">
        <div class="container">
            <form class="form-signin" method="POST">
                <h2 class="form-signin-heading" style="background: #2a3542">JHTTP</h2>
                <div class="login-wrap">
                    <input style="color: #555" type="text" class="form-control" placeholder="Username" name="user" autofocus>
                    <input style="color: #555" type="password" class="form-control" name="password" placeholder="Password">
                    <br/>
                    <br/>
                    <button style="text-transform: none; background: #5bc0de; box-shadow: none;" name="a" value="Login or Register" class="btn btn-lg btn-login btn-block" type="submit">Login</button>
                </div>
            </form>
        </div>
    </body>
</html>