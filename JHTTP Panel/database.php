<?php
    require_once 'configuration.php';
    session_start();
    if(!isset($_SESSION['account']))
    {
        header("Location: login.php");
        die();
    }
    $database = new mysqli($DATABASE_SERVER, $DATABASE_USER, $DATABASE_PASSWORD, $DATABASE_NAME);
    $database->query("CREATE TABLE IF NOT EXISTS `clients` (`ip` varchar(1024) NOT NULL default'', `hardwarescore` varchar(1024) NOT NULL default '', `location` varchar(1024), `ping` INTEGER NOT NULL default 0, `os` varchar(1024) NOT NULL default '', `time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP)");
?>
