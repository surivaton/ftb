-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 22, 2012 at 12:38 AM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `vpos`
--

-- --------------------------------------------------------

--
-- Table structure for table `cmdresults`
--

CREATE TABLE IF NOT EXISTS `cmdresults` (
  `id` bigint(16) unsigned NOT NULL AUTO_INCREMENT,
  `taskId` bigint(16) NOT NULL,
  `botId` bigint(16) NOT NULL,
  `taskStatus` enum('0','1','2') NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `commands`
--

CREATE TABLE IF NOT EXISTS `commands` (
  `taskId` bigint(16) unsigned NOT NULL AUTO_INCREMENT,
  `taskNote` varchar(32) NOT NULL,
  `taskType` enum('dlx','upd') NOT NULL,
  `taskURL` varchar(260) NOT NULL,
  `taskLimit` int(16) NOT NULL DEFAULT '0',
  `taskCountry` varchar(260) NOT NULL,
  `taskBuildId` varchar(260) NOT NULL,
  PRIMARY KEY (`taskId`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `terminals`
--

CREATE TABLE IF NOT EXISTS `terminals` (
  `id` bigint(16) unsigned NOT NULL AUTO_INCREMENT,
  `botIp` varchar(16) CHARACTER SET utf8 NOT NULL,
  `botId` varchar(32) CHARACTER SET utf8 NOT NULL,
  `buildId` varchar(16) NOT NULL,
  `buildVer` varchar(7) CHARACTER SET utf8 NOT NULL,
  `osVer` varchar(7) CHARACTER SET utf8 NOT NULL,
  `cName` varchar(32) NOT NULL,
  `cUser` varchar(32) NOT NULL,
  `cAdmin` enum('0','1') CHARACTER SET utf8 NOT NULL,
  `lastTime` int(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
