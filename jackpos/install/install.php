<?php header('Content-Type: text/html; charset=utf-8'); ?>
<html>
<!--  #####################################################################################  -->
<!--  ############## (¯`·._.·[ JackPos quick'n'dirty installer v0.1 ]·._.·´¯) #############  -->
<!--  #####################################################################################  -->
<head> 
<script src="../resources/scripts/codef_core.js"></script>
<script src="../resources/scripts/codef_fx.js"></script>
<script src="../resources/scripts/codef_music.js"></script>
<script> 
var player = new music("YM");
player.LoadAndRun("../resources/images/if pigs could fly.ym");

var myCanvas;
var myBuffer;
var myimage=new image('../resources/images/logo.black.png');
var myfx;
var myfxparam=[
		{value:0, amp:30, inc:0.02, offset:0.05},
		{value:0, amp:30, inc:0.01, offset:0.08}
	      ];

var fadein;
var fadein_rgb=255;
 
function init(){
	myCanvas=new canvas(640, 480, "main");
	myBuffer=new canvas(302,179);
	fadein=new canvas(302, 179);
	myfx=new FX(myBuffer,myCanvas,myfxparam);
	go();
}
 
function go(){
	myCanvas.clear();
	myBuffer.clear();
	
	if (fadein_rgb>0)
	{
		fadein_rgb=fadein_rgb-1;
		fadein.fill(rgbToHex(fadein_rgb, fadein_rgb, fadein_rgb));
		myimage.draw(myBuffer,0,0);
		myBuffer.contex.globalCompositeOperation='source-in';
		fadein.draw(myBuffer, 0, 0);
		myBuffer.contex.globalCompositeOperation='source-over';
	} else {
		myimage.draw(myBuffer,0,0);
	}

	myfx.sinx(169,150);
	
	requestAnimFrame(go);
}

function intTo2Hex(c) {
    var hex = c.toString(16);
    return hex.length == 1 ? "0" + hex : hex;
}

function rgbToHex(r, g, b) {
    return "#" + intTo2Hex(r) + intTo2Hex(g) + intTo2Hex(b);
}
</script> 

		<title>JackPos quick'n'dirty installer v0.1</title>
		<style>
			body {
				background-color: #FFFFFF;
				color:            white;
			}
			
			h1 {
				text-align: center;
			}
			
			h2 {
				text-align: center;
				color: lime;
			}
			
			input {
				width: 100%;
			}
		body,td,th {
	color: #000000;
}
</style>
	</head>
<body onLoad="init();">
<?php
	if (file_exists('../config.php')) {
		die('Already installed!');
	}
	
	if (exists(array('host', 'username', 'password', 'database', 'p'))) {
		$host     = $_POST['host'];
		$username = $_POST['username'];
		$password = $_POST['password'];
		$database = $_POST['database'];
		$p        = $_POST['p'];
		
		$sql    = file_get_contents('jack.sql');
		$sql    = str_replace('{ADMIN_PWD}', md5($p), $sql);
		
		$config    = file_get_contents('config.inc');
		$config    = str_replace('{HOST}', $host,     $config);
		$config    = str_replace('{USER}', $username, $config);
		$config    = str_replace('{PASS}', $password, $config);
		$config    = str_replace('{DB}',   $database, $config);
		
		file_put_contents('../config.php', $config);
		
		mysql_connect($host, $username, $password);
		mysql_select_db($database);
		
		$sql = explode(';', $sql);
		
		foreach ($sql as $line) {
			$_ = trim($line);
			
			if (strlen($_) <= 0)
				continue;
			
			mysql_query($_);
		}
		
		mysql_close();
		
		unlink('jack.sql');
		unlink('config.inc');
		unlink('install.php');
		rmdir('.');
		
		$message = 'JackPos installed and installer removed!';
	}
	
	function exists($array) {
		foreach ($array as $entry) {
			if (!isset($_POST[$entry]) || !is_string($_POST[$entry])) {
				return false;
			}
		}
		
		return true;
	}
	
?>
<center>
<h1>&#x269B; JackPos quick'n'dirty installer v0.1 &#x269B;</h1>
<?php
	if (isset($message)) {
?>
		<h2><?php echo($message); ?></h2>
<?php
	}
?>
		
		<form action="install.php" method="POST">
			<h3>MySQL</h3>
			
			<input type="text"     name="host"     placeholder="Host"     autocomplete="off" autofocus /><br />
			<input type="text"     name="username" placeholder="Username" autocomplete="off" /><br />
			<input type="password" name="password" placeholder="Password" autocomplete="off" />
			<input type="text"     name="database" placeholder="Database" autocomplete="off" />
			
			<h3>Panel</h3>
			
			<input type="password" name="p" placeholder="Password" autocomplete="off" /><br />
			
			<br />
			
			<input type="submit" value="Install!" />
		</form>
<div id="main"></div></center>
	</body>
</html>