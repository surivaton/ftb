<?php
function GetStats($hSql, $dwOnlineSeconds)
{
    $sHtml = file_get_contents("inc/stats.html");


    $dwResult = mysql_query("SELECT (SELECT count(*) FROM b_bots) AS bots,
							(SELECT count(*) FROM b_bots WHERE Last >" . (time() - ($dwOnlineSeconds+5)) . ") AS bots_online,
							(SELECT count(*) FROM b_bots WHERE Last >" . (time() - 86400) . ") AS bots_online24,
							(SELECT count(*) FROM b_bots WHERE Windows = '2.5.1') AS bots_XP,
							(SELECT count(*) FROM b_bots WHERE Windows = '2.5.2') AS bots_2003,
							(SELECT count(*) FROM b_bots WHERE Windows = '2.6.0') AS bots_Vista,
							(SELECT count(*) FROM b_bots WHERE Windows = '2.6.1') AS bots_Seven,
							(SELECT count(*) FROM b_bots WHERE Windows = '2.6.2') AS bots_8,
							(SELECT count(*) FROM b_headers) AS logs,
							(SELECT count(*) FROM b_headers WHERE UCASE(Program) = 'IEXPLORE.EXE') AS logs_IE,
							(SELECT count(*) FROM b_headers WHERE (UCASE(Program) = 'FIREFOX.EXE') or (UCASE(Program) = 'TBB-FIREFOX.EXE')) AS logs_FF,
							(SELECT count(*) FROM b_headers WHERE UCASE(Program) = 'CHROME.EXE') AS logs_CH", $hSql);
							
    $row      = mysql_fetch_array($dwResult);
	
	//Process Bots
	if ($row['bots'] != 0) {
		$sHtml = str_replace("%TOTAL%", $row['bots'], $sHtml);
		$sHtml = str_replace("%ONLINE%", $row['bots_online'], $sHtml);
		$sHtml = str_replace("%ONLINE24%", $row['bots_online24'], $sHtml);
		$sHtml = str_replace("%OFFLINE%", $row['bots'] - $row['bots_online'], $sHtml);
		$sHtml = str_replace("%OFFLINE24%", $row['bots'] - $row['bots_online24'], $sHtml);
		
		$sHtml = str_replace("%PONLINE%", Round(($row['bots_online'] / $row['bots']) * 100), $sHtml);
		
		$sHtml = str_replace("%XP%", $row['bots_XP'], $sHtml);
		$sHtml = str_replace("%2003%", $row['bots_2003'], $sHtml);
		$sHtml = str_replace("%VISTA%", $row['bots_Vista'], $sHtml);
		$sHtml = str_replace("%7%", $row['bots_Seven'], $sHtml);
		$sHtml = str_replace("%8%", $row['bots_8'], $sHtml);
		$sHtml = str_replace("%UNK%", $row['bots'] - $row['bots_XP'] - $row['bots_2003'] - $row['bots_Vista'] - $row['bots_Seven'] - $row['bots_8'], $sHtml);
	}else{
		$sHtml = str_replace("%TOTAL%", "0", $sHtml);
		$sHtml = str_replace("%ONLINE%", "0", $sHtml);
		$sHtml = str_replace("%ONLINE24%", "0", $sHtml);
		$sHtml = str_replace("%OFFLINE%", "0", $sHtml);
		$sHtml = str_replace("%OFFLINE24%", "0", $sHtml);
		
		$sHtml = str_replace("%PONLINE%", "0", $sHtml);

		$sHtml = str_replace("%XP%", "0", $sHtml);
		$sHtml = str_replace("%2003%", "0", $sHtml);
		$sHtml = str_replace("%VISTA%", "0", $sHtml);
		$sHtml = str_replace("%7%", "0", $sHtml);
		$sHtml = str_replace("%8%", "0", $sHtml);
		$sHtml = str_replace("%UNK%", "0", $sHtml);	
	}
	
	//Process Logs
	if ($row['logs'] != 0) {
		$sHtml = str_replace("%IE%", $row['logs_IE'], $sHtml);
		$sHtml = str_replace("%FF%", $row['logs_FF'], $sHtml);
		$sHtml = str_replace("%CH%", $row['logs_CH'], $sHtml);
		$sHtml = str_replace("%FOTH%", $row['logs'] - $row['logs_IE'] - $row['logs_FF'] - $row['logs_CH'], $sHtml);
	}else{
		$sHtml = str_replace("%IE%", "0", $sHtml);
		$sHtml = str_replace("%FF%", "0", $sHtml);
		$sHtml = str_replace("%CH%", "0", $sHtml);
		$sHtml = str_replace("%FOTH%", "0", $sHtml);
	}

    
    Return $sHtml;
}
?>