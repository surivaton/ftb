<?php
function ExportDB($hSql, $sTable){
	//run mysql query and then count number of fields
	$export = mysql_query ("SELECT * FROM `" . $sTable . "`", $hSql) or die ( "Sql error : " . mysql_error( ) );
	$fields = mysql_num_fields ($export);

	//create csv header row, to contain table headers with database field names
	for ( $i = 0; $i < $fields; $i++ ) {
		$header .= mysql_field_name($export, $i) . ",";
	}

	//Loop through the query results, and create 
	//a row for each
	while( $row = mysql_fetch_row($export) ) {
		$line = '';
		//for each field in the row
		foreach( $row as $value ) {
			//if null, create blank field
			if ( ( !isset( $value ) ) || ( $value == "" ) ){
				$value = ",";
			}
			//else, assign field value to our data
			else {
				$value = str_replace( '"' , '""' , $value );
				$value = '"' . $value . '"' . ",";
			}
			//add this field value to our row
			$line .= $value;
		}
		//trim whitespace from each row
		$data .= trim( $line ) . "\n";
	}
	//remove all carriage returns from the data
	$data = str_replace( "\r" , "" , $data );


	//create a file and send to browser for user to download
	header("Content-type: application/vnd.ms-excel");
	header("Content-disposition: csv" . date("Y-m-d") . ".csv");
	header("Content-disposition: filename=" . $sTable . ".csv");
	print "$header\n$data";
	exit;
}
?>
