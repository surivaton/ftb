<?php include('auth.php'); ?>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <title>Umbra Loader</title>
    <link rel="stylesheet" type="text/css" href="../resources/css/ext-all.css" />
 	<script type="text/javascript" src="../adapter/ext/ext-base.js"></script>
    <script type="text/javascript" src="../ext-all.js"></script>
    <link rel="stylesheet" type="text/css" href="css/desktop.css" />
    <link rel="stylesheet" href="../Ext.ux.form.CheckboxCombo/Ext.ux.form.CheckboxCombo.min.css" />
	<script type="text/javascript" src="../Ext.ux.form.CheckboxCombo/Ext.ux.form.CheckboxCombo.min.js"></script>

	<link rel="stylesheet" type="text/css" href="../plugins/fileuploadfield.css"/>
	<script type="text/javascript" src="../plugins/FileUploadField.js"></script>
	<!-- start include awesomeuploader plugins -->
	<link rel="stylesheet" type="text/css" href="../plugins/awesomeuploader/Ext.ux.form.FileUploadField.css" />
	<script type="text/javascript" src="../plugins/awesomeuploader/Ext.ux.form.FileUploadField.js"></script>
	<script type="text/javascript" src="../plugins/awesomeuploader/Ext.ux.XHRUpload.js"></script>
	
    
    <link rel="Stylesheet" type="text/css" href="style.css" media="screen" />
    <script type="text/javascript" src="js/StartMenu.js"></script>
    <script type="text/javascript" src="js/TaskBar.js"></script>
    <script type="text/javascript" src="js/Desktop.js"></script>
    <script type="text/javascript" src="js/App.js"></script>
    <script type="text/javascript" src="js/Module.js"></script>
    <script type="text/javascript" src="../ux/SlidingPager.js"></script>
    <script type="text/javascript" src="../ux/PanelResizer.js"></script>
    <script type="text/javascript" src="../ux/PagingStore.js"></script>
    <script type="text/javascript" src="list-view.js"></script>
    <script type="text/javascript" src="plugins.js"></script>
</head>
<body>
    <body scroll="no">
    <div id="x-desktop">
        <dl id="x-shortcuts">
            <dt id="country-win-shortcut">
                <a href="#"><img src="images/s.gif" />
                <div>Country Statistics</div></a>
            </dt>
            <dt id="command-win-shortcut">
                <a href="#"><img src="images/s.gif" />
                <div>Commands</div></a>
            </dt>
            <dt id="bots-win-shortcut">
                <a href="#"><img src="images/s.gif" />
                <div>Bots</div></a>
            </dt>
            <dt id="install-win-shortcut">
                <a href="#"><img src="images/s.gif" />
                <div>Installs</div></a>
            </dt>
			<dt id="update-win-shortcut">
                <a href="#"><img src="images/s.gif" />
                <div>File Upload</div></a>
            </dt>
        </dl>
    </div>
    <div id="ux-taskbar">
        <div id="ux-taskbar-start"></div>
        <div id="ux-taskbuttons-panel"></div>
        <div class="x-clear"></div>
    </div>
    
</body>
</html>
