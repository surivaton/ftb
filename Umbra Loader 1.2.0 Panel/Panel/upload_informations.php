<?php
	include('auth.php');
	function getKB( $iByte, $iAccuracy = 2 )
	{
		$iKB = $iByte / 1000;
		$fKB = sprintf ( '%.' . $iAccuracy . 'f', $iKB );
		return rtrim ( rtrim ( $fKB, '0' ), '.' );
	} 
	$filename = "uploads/server.exe";
	if (file_exists($filename)){
		$int_filesize = filesize($filename);
		$int_filesize = getKB($int_filesize,3);
		
		$str_md5 = md5_file($filename);
		$str_date =  date("F d Y H:i:s.", filemtime($filename));
		if (($int_filesize) && ($str_md5) && ($str_date)){
			die("<br>Binary found in /uploads/<br>Filesize: ".$int_filesize." KBytes <br>MD5: ".$str_md5."<br>Last modified: ".$str_date);
		}
		else{
			die("Couldnt find binary!");
		}
	}
	else{
		die("There is no binary uploaded!");
	}
?>
